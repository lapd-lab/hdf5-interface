/* Call Library source file */
/*
/**/

#include "extcode.h"
#include "hdf5.h"
#include "stdio.h"
#include "H5Spublic.h"

extern "C"
{
	_declspec(dllexport) long call_H5Sget_simple_extent_dims( long space_id );

}

_declspec(dllexport) long call_H5Sget_simple_extent_dims( long space_id )
{
    FILE *stream = fopen( "C:/Temp/call_H5Sget_simple_extent_dims.txt", "w" );
	hsize_t dims[256];
	hsize_t maxdims[256];
	//long dims[256];
	//long maxdims[256];
	//int ndims = H5Sget_simple_extent_dims( space_id, dims, maxdims );
	int ndims = 2;
	dims[0] = 500;
	dims[1] = 1024;
	maxdims[0] = 500;
	maxdims[1] = 1024;
	for ( int i=0; i<ndims; i++ )
		fprintf( stream, "%d %d\n", dims[i], maxdims[i] );

	fclose( stream );

	return( 0 );
}