/* Call Library source file */
/*
/* It turns out that we don't need this.  The problem was that hsize_t is "unsigned __int64"
/**/

#include "extcode.h"
#include "hdf5.h"
/* extern unsigned H5P_CLS_FILE_ACCESS_g; */

extern "C"
{
	_declspec(dllexport) hsize_t H5Sget_simple_extent_dim(long space_id, long idx);

}

_declspec(dllexport) hsize_t H5Sget_simple_extent_dim(long space_id, long idx)
{
	hsize_t dims[256];
	hsize_t maxdims[256];
	long ndims = H5Sget_simple_extent_dims( space_id, dims, maxdims );

	if ((idx >= 0) && (idx < ndims) )
	{
		hsize_t dim = dims[idx];
		return( dim );
	}

	return( 0 );
}