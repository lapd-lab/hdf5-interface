//========================================================================================
// Insert devices.h
//

#include "hdf5.h"


herr_t find_dataset(
	hid_t file_id,
	char *group_name,
	char *dataset_name,
	hid_t *group_id,
	bool *found);

herr_t extend_create_2D_dataset(
	char* dataset,
	hid_t type_id,
	bool found,
	hid_t group_id,
	int index,
	int data_length,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t extend_create_header_dataset(
	char* dataset,
	char* header_type,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t extend_create_remote_header(
	char* dataset,
	char* device_type,
	int data_type,
	char* descriptor,
	int header_bytes,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t insert_devices(
	_TCHAR* argv[],
	hid_t file_id,
	int shot_number,
	int* devices_index,
	int* error_index);


