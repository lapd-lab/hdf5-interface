//========================================================================================
// Error handling.h
//
// In all cases, the shot number is prepended and a new line is appended to the error
// string.
//
#include "stdio.h"
//#include "hdf5.h"
#include <H5public.h>
#include <H5Apublic.h>
#include <H5Epublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>

void write_error( char* efn, int shot_number, char* error_string );
void write_hdf5_error( char* efn, int shot_number );

void insert_error( hid_t file_id, int *error_index, int shot_number, char* error_string );
void insert_hdf5_error( hid_t file_id, int *error_index, int shot_number );

herr_t H5Dinsert_error_string( hid_t file_id, int* error_index, char* error_string );
