//========================================================================================
// Data structures.h : Various data structures to match the LabVIEW clusters.
//
//


#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H


struct dischargeStruct
{
	int current_count;
	float *current;
	int voltage_count;
	float *cathode_anode_voltage;
	char summary_buffer[25];
};

struct heaterStruct
{
	char summary_buffer[25];
};

struct pressureStruct
{
	int pressure_count;
	float *RGA_partial_pressure;
	char summary_buffer[22];
};

struct magneticFieldStruct
{
	int profile_count;
	float *profile;
	int current_count;
	float *supply_current;
	char summary_buffer[17];
};

struct interferometerStruct
{
	int density_count;
	float *density;
	char summary_buffer[17];
};

struct dataElement
{
	char LV_type;
	char* name;
	short dims;
	short enum_count;
	char** enum_names;
	short sub_element_count;
	short* sub_elements;
	dataElement* sub_reference_list;
	short sub_descriptor_count;
};


// Data structures for the "Average SIS crate" function call: something to hold one channel of 
//   SIS crate average shot data, plus various other random things.

struct subChannelStruct
{
	int ndata;
	unsigned int *data;  // has to be int to allow for summing without overflow
};


struct channelStruct
{
	char channel_name[1000];
	unsigned short channel_mode;
	short average_count;
	short average_shot_number;
	float vertical_scale;
	float vertical_offset;
	char enabled_code;
	int nU32;
	subChannelStruct sub_channels[4];
};


static char* channel_modes[4] =
{
	"4 sub-channels possible",
	"2 sub-channels possible",
	"1 sub-channel possible",
	"1 sub-channel possible"
};


static char* enabled_codes[16] =
{
	"0000",
	"0001",
	"0010",
	"0011",
	"0100",
	"0101",
	"0110",
	"0111",
	"1000",
	"1001",
	"1010",
	"1011",
	"1100",
	"1101",
	"1110",
	"1111"
};


static char* sub_channel_suffixes[4] =
{
	" ch 1",
	" ch 2",
	" ch 3",
	" ch 4"
};


#endif

