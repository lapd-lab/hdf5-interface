//========================================================================================
// Error handling.cpp
//


#include "stdafx.h"


//----------------------------------------------------------------------------------------
// Write error
//
void write_error( char* efn, int shot_number, char* error_string )
{
	FILE* error_file;
	errno_t fopen_err = fopen_s( &error_file, efn, "a" );  if ( fopen_err ) return;
	fprintf( error_file, "Shot %d: %s\n", shot_number, error_string );
	fclose( error_file );

	fprintf( stderr, "%s\n", error_string );
}


void write_hdf5_error( char* efn, int shot_number )
{
	FILE* error_file;
	errno_t fopen_err = fopen_s( &error_file, efn, "a" );  if ( fopen_err ) return;
	fprintf( error_file, "Shot %d: ", shot_number );
	H5Eprint2( H5E_DEFAULT, error_file );
	fclose( error_file );

	H5Eprint2( H5E_DEFAULT, stderr );
}


//----------------------------------------------------------------------------------------
// Insert error
//
void insert_error( hid_t file_id, int* error_index, int shot_number, char* error_string )
{
	char full_error_string[4096];
	sprintf_s( full_error_string, 4096, "Shot %d: %s", shot_number, error_string );
	H5Dinsert_error_string( file_id, error_index, full_error_string );
}


void insert_hdf5_error( hid_t file_id, int* error_index, int shot_number )
{
	FILE* temp_error_file;
	errno_t fopen_err = fopen_s( &temp_error_file, "C:/Temp/temp_error_file.dat", "w" );  if ( fopen_err ) return;
	fprintf( temp_error_file, "Shot %d: ", shot_number );
	H5Eprint2( H5E_DEFAULT, temp_error_file );
	fclose( temp_error_file );

	fopen_err = fopen_s( &temp_error_file, "C:/Temp/temp_error_file.dat", "r" );  if ( fopen_err ) return;
	char full_error_string[4096];
	for ( int i=0; i<4096; i++ )
	{
		if ( feof(temp_error_file) != 0 )
		{
			full_error_string[i] = '\0';
			break;
		}
		fread( &(full_error_string[i]), 1, 1, temp_error_file );
	}
	fclose( temp_error_file );

	H5Dinsert_error_string( file_id, error_index, full_error_string );
}



//----------------------------------------------------------------------------------------
// H5Dinsert_error_string
//
herr_t H5Dinsert_error_string( hid_t file_id, int* error_index, char* error_string )
{
	char group[1024] = "/";
	char dataset[1024] = "Error list";
	int index = *error_index;


	// ****************************
	// *** Look for the dataset ***
	//
	// Open the containing group and get the number of objects
	//
	hid_t group_id = H5Gopen2( file_id, group, H5P_DEFAULT );
	if ( group_id < 0 ) return -1;

	hsize_t num_obj;
	herr_t err = H5Gget_num_objs( group_id, &num_obj );

	// Loop through objects, looking for a match with the dataset name
	//
	char obj_name[1024];
	bool found = false;
	if ( err == 0 )
		for ( hsize_t idx=0; idx<num_obj; idx++ )
		{
			ssize_t size = H5Gget_objname_by_idx( group_id, idx, obj_name, 1024 );
			if ( size < 0 ) { err = -1; break; }
			if ( strcmp(obj_name, dataset) == 0 ) { found = true; break; }
		}


	// Declare the id's that we'll need to write into the dataset
	//
	hid_t dataset_id;
	hid_t file_space_id;
	hid_t mem_space_id;
	hid_t mem_type_id;


	// ************************************************
	// *** If the dataset does not exist, create it ***
	//
	if ( (found == false) && (err == 0) )
	{
		// File dataspace
		//
		int rank = 1;
		hsize_t file_dims[1] = { index+1 };
		hsize_t file_maxdims[1] = { 4294967295 };
		file_space_id = H5Screate_simple( rank, file_dims, file_maxdims );
		if ( file_space_id < 0 ) err = -1;

		hsize_t start[1] = { index };
		hsize_t stride[1] = { 1 };
		hsize_t count[1] = { 1 };
		hsize_t block[1] = { 1 };

		if ( err == 0 )
			err = H5Sselect_hyperslab( file_space_id, H5S_SELECT_SET, start, stride, count, block );


		// Memory dataspace
		//
		hsize_t mem_dims[1] = { 1 };
		hsize_t mem_maxdims[1] = { 1 };

		if ( err == 0 )
		{
			mem_space_id = H5Screate_simple( rank, mem_dims, mem_maxdims );
			if ( mem_space_id < 0 ) err = -1;
		}


		// Dataset creation property list
		//
		hid_t plist_id = 0;

		if ( err == 0 )
		{
			plist_id = H5Pcreate( H5P_DATASET_CREATE );
			if ( plist_id < 0 ) err = -1;
		}

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		if ( err == 0 )
		{
			H5Pset_chunk( plist_id, rank, file_chunk );
			H5Pset_deflate( plist_id, compression );
		}


		// Memory datatype
		//
		if ( err == 0 )
		{
			mem_type_id = H5Tcopy( H5T_C_S1 );
			if ( mem_type_id < 0 ) err = -1;
		}

		if ( err == 0 )
			err = H5Tset_size( mem_type_id, 4096 );


		// Everything is set up so now create the 1D string dataset
		//
		if ( err == 0 )
		{
			dataset_id = H5Dcreate( group_id, dataset, mem_type_id, file_space_id, plist_id, H5P_DEFAULT, H5P_DEFAULT );
			if ( dataset_id < 0 ) err = -1;
		}

		H5Pclose( plist_id );
	}


	// ********************************************
	// *** If the dataset does exist, extend it ***
	//
	if ( (found == true) && (err == 0) )
	{
		dataset_id = H5Dopen( group_id, dataset, H5P_DEFAULT );
		if ( dataset_id < 0 ) err = -1;


		// File dataspace
		//
		if ( err == 0 )
		{
			file_space_id = H5Dget_space( dataset_id );
			if ( file_space_id < 0 ) err = -1;
		}

		int rank = 1;
		hsize_t file_dims[1] = { index+1 };
		hsize_t file_maxdims[1] = { 4294967295 };

		if ( err == 0 )
			err = H5Sset_extent_simple( file_space_id, rank, file_dims, file_maxdims );

		hsize_t start[1] = { index };
		hsize_t stride[1] = { 1 };
		hsize_t count[1] = { 1 };
		hsize_t block[1] = { 1 };

		if ( err == 0 )
			err = H5Sselect_hyperslab( file_space_id, H5S_SELECT_SET, start, stride, count, block );


		// Memory dataspace
		//
		hsize_t mem_dims[1] = { 1 };
		hsize_t mem_maxdims[1] = { 1 };
		if ( err == 0 )
		{
			mem_space_id = H5Screate_simple( rank, mem_dims, mem_maxdims );
			if ( mem_space_id < 0 ) err = -1;
		}


		// Extend the dataset
		//
		if ( err == 0 )
			err = H5Dextend( dataset_id, file_dims );


		// Memory datatype
		//
		if ( err == 0 )
		{
			mem_type_id = H5Tcopy( H5T_C_S1 );
			if ( mem_type_id < 0 ) err = -1;
		}

		if ( err == 0 )
			err = H5Tset_size( mem_type_id, 4096 );
	}


	// *****************************************
	// *** Write the string into the dataset ***
	//
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, error_string );
	if ( err == 0 )
		*error_index = index + 1;

	H5Dclose( dataset_id );
	H5Tclose( mem_type_id );

	H5Sclose( mem_space_id );
	H5Sclose( file_space_id );

	H5Gclose( group_id );

	return err;
}



