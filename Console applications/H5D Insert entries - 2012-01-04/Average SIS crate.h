//========================================================================================
// Average SIS crate.h
//

#include "hdf5.h"
#include "Data structures.h"


void init_3302_data( 
	channelStruct &channel );

void init_3305_data( 
	channelStruct &channel );

void assign_3302_data( 
	channelStruct &channel,
	int *raw_U32_data );

void sum_3302_data( 
	channelStruct &channel,
	int *raw_U32_data );

void assign_3305_data( 
	channelStruct &channel,
	int *raw_U32_data );

void sum_3305_data( 
	channelStruct &channel,
	int *raw_U32_data );

int find_channel( 
	channelStruct *channels,
	int full_channel_count,
	char *channel_name );

void form_average( 
	channelStruct *channels,
	int full_channel_count );

int write_shot_file( 
	FILE *shot_file,
	char *configuration_name,
	channelStruct *channels,
	int full_channel_count );

int read_average_shot_file(
	FILE *average_shot_file,
	char *prev_configuration_name,
	channelStruct *channels,
	int &full_channel_count,
	short average_shot_number_in,
	short &max_average_count );

herr_t average_SIS_crate(
	char *data_run_folder,
	char *data_run,
	int shot_number );
