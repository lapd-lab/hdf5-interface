//========================================================================================
// Insert sequence lines.cpp
//

#include "stdafx.h"


//----------------------------------------------------------------------------------------
// Extend/Create sequence dataset
//
herr_t extend_create_sequence_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	int line_count,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	herr_t err = 0;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen2( group_id, dataset, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;
	}


	// File dataspace
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { index+line_count };
	hsize_t file_maxdims[1] = { 4294967295 };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) return -1;
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) return -1;
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) return -1;
	}

	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { line_count };
	hsize_t block[1] = { 1 };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) return -1;


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { line_count };
	hsize_t mem_maxdims[1] = { line_count };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) return -1;


	// Memory datatype
	//
	hid_t char_field_id = H5Tcopy( H5T_NATIVE_CHAR );
	hid_t short_field_id = H5Tcopy( H5T_NATIVE_SHORT );
	hid_t int_field_id = H5Tcopy( H5T_NATIVE_INT );
	hid_t float_field_id = H5Tcopy( H5T_NATIVE_FLOAT );
	hid_t double_field_id = H5Tcopy( H5T_NATIVE_DOUBLE );
	hid_t string120_field_id = H5Tcopy( H5T_C_S1 );  H5Tset_size( string120_field_id, 120 );
	hid_t string20_field_id = H5Tcopy( H5T_C_S1 );  H5Tset_size( string20_field_id, 20 );

	int byte_count = 156;
	*mem_type_id = H5Tcreate( H5T_COMPOUND, byte_count );
	if ( *mem_type_id < 0 ) return -1;
	err = H5Tinsert( *mem_type_id, "Line", 0, string120_field_id );  if ( err < 0 ) return -1;
	err = H5Tinsert( *mem_type_id, "Shot number", 120, int_field_id );  if ( err < 0 ) return -1;
	err = H5Tinsert( *mem_type_id, "Expanded line number", 124, int_field_id );  if ( err < 0 ) return -1;
	err = H5Tinsert( *mem_type_id, "Line status", 128, string20_field_id );  if ( err < 0 ) return -1;
	err = H5Tinsert( *mem_type_id, "Time stamp", 148, double_field_id );  if ( err < 0 ) return -1;

	H5Tclose( char_field_id );  H5Tclose( short_field_id );  H5Tclose( int_field_id );  H5Tclose( float_field_id );
	H5Tclose( double_field_id );  H5Tclose( string120_field_id );  H5Tclose( string20_field_id );


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) return -1;
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) return -1;

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate2( group_id, dataset, *mem_type_id, *file_space_id, plist_id, H5P_DEFAULT, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;

		H5Pclose( plist_id );
	}

	return 0;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Insert sequence lines
//
herr_t insert_sequence_lines(
	_TCHAR* argv[],
	hid_t file_id,
	int shot_number,
	int* sequence_lines_index,
	int* error_index )
{
	// Read in the needed arguments
	//
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];

	strcpy_s( experiment_set, 1024, argv[2] );
	strcpy_s( experiment, 1024, argv[3] );
	strcpy_s( data_run, 1024, argv[4] );

	char root_folder[1024] = "C:/Shadow data";
	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf_s( experiment_folder, 1024, "%s/%s/%s", root_folder, experiment_set, experiment );
	sprintf_s( data_run_folder, 1024, "%s/%s", experiment_folder, data_run );

	char efn[1024];  // efn == error_filename, shortened to allow single line error-handling
	sprintf_s( efn, 1024, "%s/Error.C++.dat", data_run_folder );


	// Deal with shot number
	//
	int* shot_number_ptr = (int*) malloc( sizeof(int) );
	char* shot_number_bytes;
	*shot_number_ptr = shot_number;
	shot_number_bytes = (char*) shot_number_ptr;
	int index = *sequence_lines_index;


	// ************************************************
	// *** Open the sequence lines file and read it ***
	//
	// Open the sequence lines file
	//
	char sequence_lines_filename[1024];
	sprintf_s( sequence_lines_filename, 1024, "%s/Sequence lines/Sequence lines.%06d.dat", data_run_folder, shot_number );
	FILE* sequence_lines_file;
	errno_t fopen_err = fopen_s( &sequence_lines_file, sequence_lines_filename, "rb" );  if ( fopen_err ) return -1;


	// Get the line count, then loop through the lines.
	//
	int character_count = read_int( sequence_lines_file );
	int line_count = read_int( sequence_lines_file );
	int byte_count = 156;
	char *sequence_summary = (char *)malloc( line_count*byte_count );
	for ( int i=0; i<line_count; i++ )
	{
		// Read sequence line structure (for each line)
		//
		read_string( sequence_lines_file, &(sequence_summary[0+i*byte_count]), 120 );  // sequence line
		read_int_bytes( sequence_lines_file, &(sequence_summary[120+i*byte_count]) );  // shot_number
		read_int_bytes( sequence_lines_file, &(sequence_summary[124+i*byte_count]) );  // expanded_line_number
		read_string( sequence_lines_file, &(sequence_summary[128+i*byte_count]), 20 );  // line_status
		read_double_bytes( sequence_lines_file, &(sequence_summary[148+i*byte_count]) );  // time_stamp
	}


	// ************************************
	// *** Process the sequence dataset ***
	//
	// This involves:
	//   1) looking for the dataset
	//   2) if found, extending it
	//   3) if not found, create it
	//   4) writing the lines into it
	//   5) closing the id's that were opened along the way
	//
	// First, though, declare the id's that we'll need
	//
	hid_t group_id;
	hid_t dataset_id;
	hid_t file_space_id;
	hid_t mem_space_id;
	hid_t mem_type_id;

	herr_t err = 0;
	bool found;

	if ( err == 0 )
		err = find_dataset( file_id, "/Raw data + config/Data run sequence", "Data run sequence", &group_id, &found );
	if ( err == 0 )
		err = extend_create_sequence_dataset( "Data run sequence", found, group_id, 
			index, line_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err == 0 )
		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, sequence_summary );
	if ( err < 0 )
		{ write_hdf5_error( efn, shot_number ); insert_hdf5_error( file_id, error_index, shot_number ); }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// **********************************************
	// *** Free up HDF5 id's and allocated memory ***
	//
	H5Gclose( group_id );
	fclose( sequence_lines_file );

	free( shot_number_ptr );
	free( sequence_summary );
	*sequence_lines_index = index + line_count;

	return err;
}


