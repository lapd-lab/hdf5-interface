//========================================================================================
// Insert MSI - generic.h
//

#include "hdf5.h"
#include "..\jsoncpp\json\json.h"


herr_t extend_create_2D_MSI_dataset_POC(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	int data_length,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t extend_create_summary_dataset_POC(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t insert_MSI_generic(
	_TCHAR* argv[],
	char* data_run_id,
	hid_t file_id,
	int shot_number,
	int* MSI_index,
	int* error_index);

void update_HDF5_object(
	hid_t loc_id,
	Json::Value MSI_HDF5_json,
	int* MSI_index,
	int* error_index);


