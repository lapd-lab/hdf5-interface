//========================================================================================
// Insert MSI POC caller.cpp : Defines the entry point for the console application.
//
// Usage:
//   Insert_MSI_POC_caller.exe <HDF5 file> <experiment set> <experiment> <data run> <shadow data root folder> <shot number> <MSI index>
//
// Example: "C:\ACQ II home\HDF5\Current DLL\H5D_Insert_entries.exe" "C:\Projects\UCLA\analysis and design\MSI Upgrade\MSI-POC.hdf5" UCLA "analysis and design" "MSI Upgrade" "C:\Projects" 165 4
//
// Usage for real H5D_Insert_entries.exe:
//   H5D_Insert_entries.exe <HDF5 file> <experiment set> <experiment> <data run> <shadow data root folder>
//     <device count> {<device name> list} <start time> <SIS crate root folder> <sleep count (ms)> <data run id>
//
// Example for real H5D_Insert_entries.exe:
//   "C:\ACQ II home\HDF5\Current DLL\H5D_Insert_entries.exe" "C:\Projects\UCLA\analysis and design\MSI Upgrade\MSI-POC.hdf5" Shakedown "SIS crate" "SIS crate 01" "C:\Shadow data" 1 "SIS crate" 31198877 "C:\Shadow data\SIS crate" 10 "data_run_id 237 Shakedown"
//
// <HDF5 file>: full path of the HDF5 file in which to insert a single shot of MSI
// <experiment set> <experiment> <data run>: 3 strings which locate the MSI files
// <shadow data root folder>: the root folder
//
// Writes an error file if an error occurs.
//
// It inserts a single shot of MSI data
//


#include "stdafx.h"


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	char *version = "June 8 2022";


	// Expecting "Insert_MSI_POC_caller.exe" plus 7 arguments
	//
	if ( argc != 8 )
	{
		fprintf( stdout, "Expecting \"Insert_MSI_POC_caller.exe\" plus 7 arguments.  Error exit.\n" );
		return -1;
	}


	// Read in the first 5 arguments
	//
	char HDF5_filename[1024];
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];
	char root_folder[1024];
	
	strcpy_s( HDF5_filename, 1024, argv[1] );
	strcpy_s( experiment_set, 1024, argv[2] );
	strcpy_s( experiment, 1024, argv[3] );
	strcpy_s( data_run, 1024, argv[4] );
	strcpy_s( root_folder, 1024, argv[5] );

	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf_s( experiment_folder, 1024, "%s/%s/%s", root_folder, experiment_set, experiment );
	sprintf_s( data_run_folder, 1024, "%s/%s", experiment_folder, data_run );

	char efn[1024];  // efn == error_out_filename, shortened to allow single line error-handling
	char log_filename[1024];
	sprintf_s( efn, 1024, "%s/Error.C++.dat", data_run_folder );
	sprintf_s( log_filename, 1024, "%s/Log.C++.dat", data_run_folder );


	// Print header and first 5 arguments
	//
	fprintf( stdout, "Version date: %s\n", version );
	fprintf( stdout, "Insert_MSI_POC_caller.exe beginning...\n\n" );
	fprintf( stdout, "HDF5 filename: %s\n", HDF5_filename );
	fprintf( stdout, "Experiment set: %s\n", experiment_set );
	fprintf( stdout, "Experiment: %s\n", experiment );
	fprintf( stdout, "Data run: %s\n", data_run );
	fprintf( stdout, "Root folder: %s\n", root_folder );

	FILE* log_file;
	errno_t fopen_err = fopen_s( &log_file, log_filename, "w" );  if ( fopen_err ) return -1;
	fprintf( log_file, "Version date: %s\n", version );
	fprintf( log_file, "Insert_MSI_POC_caller.exe beginning...\n\n" );
	fprintf( log_file, "HDF5 filename: %s\n", HDF5_filename );
	fprintf( log_file, "Experiment set: %s\n", experiment_set );
	fprintf( log_file, "Experiment: %s\n", experiment );
	fprintf( log_file, "Data run: %s\n", data_run );
	fprintf( log_file, "Root folder: %s\n", root_folder );


	// Setup for the loop
	//
	H5Eset_auto2( H5E_DEFAULT, NULL, NULL );  // turns off automatic printing of HDF5 errors
	hid_t file_id;

	char devices_filename[1024];
	char error_filename[1024];
	char MSI_filename[1024];
	char sequence_lines_filename[1024];
	char shot_signal_filename[1024];

	herr_t err = 0;
	int shot_number = atoi( argv[6] );
	int MSI_index = atoi(argv[7]);

	sprintf_s( error_filename, 1024, "%s/Error/Error.%06d.dat", data_run_folder, shot_number );
	sprintf_s( MSI_filename, 1024, "%s/MSI/MSI.%06d.dat", data_run_folder, shot_number );

	int error_index = 0;
	int file_timestamp = 0;
	char error_string[4096];  // dictate that error strings cannot be longer than 4096 characters
	bool error_occurred = false;


	// Open the HDF5 file
	//
	fprintf(stdout, "About to open HDF5 file: %s\n", HDF5_filename);
	file_id = H5Fopen( HDF5_filename, H5F_ACC_RDWR, H5P_DEFAULT );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
	if ( file_id < 0 )
	{
		write_hdf5_error( efn, 0 );
		fprintf( log_file, "Could not open HDF5 file.  Error exit.\n" );
		fprintf( stdout, "Could not open HDF5 file.  Error exit.\n");
		fclose( log_file );
		return -1;
	}


	// MSI
	if ( err == 0 )
	{
		FILE* MSI_file;
		fprintf(stdout, "About to open MSI file: %s\n", MSI_filename);
		fopen_err = fopen_s( &MSI_file, MSI_filename, "rb" );
		fopen_err = 0;  // Not an error if file not found
		if ( MSI_file != NULL )
		{
			fclose( MSI_file );
			fprintf( stdout, "MSI " );    fflush( stdout );
			fprintf( log_file, "MSI " );  fflush( log_file );

			err = insert_MSI_POC( argv, file_id, shot_number, &MSI_index, &error_index );

			if ( err != 0 )
			{
				error_occurred = true;
				strcpy_s( error_string, "Error inserting MSI" );
				fprintf( stdout, "-- %s", error_string );    fflush( stdout );
				fprintf( log_file, "-- %s", error_string );  fflush( log_file );

				insert_error( file_id, &error_index, shot_number, error_string );
			}
		}
	}

	// Error
	fprintf(stdout, "Write to error file if necessary\n");    fflush(stdout);
	if ( err == 0 )
	{
		FILE* error_file;
		fopen_err = fopen_s( &error_file, error_filename, "rb" );
		fopen_err = 0;  // Not an error if file not found
		if ( error_file != NULL )
		{
			fprintf( stdout, "Error " );    fflush( stdout );
			fprintf( log_file, "Error " );  fflush( log_file );
			read_string( error_file, error_string, 4096 );
			fclose( error_file );

			insert_error( file_id, &error_index, shot_number, error_string );
		}
	}


	fprintf(stdout, "Close the HDF5 file\n");    fflush(stdout);
	H5Fclose( file_id );

	fprintf( stdout, "\n" );
	fprintf( log_file, "\n" );
	fflush( stdout );

	if ( (err == 0) && (!error_occurred) )
	{
		fprintf( stdout, "Normal exit.\n" );
		fprintf( log_file, "Normal exit.\n" );
	}
	else if ( (err == 0) && (error_occurred) )
	{
		fprintf( stdout, "Normal exit (but at least 1 error occurred).\n" );
		fprintf( log_file, "Normal exit (but at least 1 error occurred).\n" );
	}
	else
	{
		fprintf( stdout, "Error exit.\n" );
		fprintf( log_file, "Error exit.\n" );
	}
	fflush( stdout );
	fclose( log_file );

	fprintf(stdout, "About to return with err: %d\n", err);    fflush(stdout);
	return err;
}


