//========================================================================================
// Read routines.cpp : Utility routines for reading from LabVIEW created strings and
// string files.
//


#include "stdafx.h"


//========================================================================================
// STRING READERS
//
//----------------------------------------------------------------------------------------
// Sread bool
//
bool sread_bool( char* string, int* index )
{
	bool value = (bool)string[*index];
	*index = *index + 1;

	return value;
}

	
//----------------------------------------------------------------------------------------
// Sread short
//
short sread_short( char* string, int* index )
{
	char value_chars[2];
	for ( int i=0; i<2; i++ ) value_chars[i] = string[*index+1-i];
	*index = *index + 2;

	short *pointer;
	pointer = (short *)value_chars;
	short value = *pointer;

	return value;
}

	
//----------------------------------------------------------------------------------------
// Sread unsigned short
//
unsigned short sread_unsigned_short(char* string, int* index)
{
	char value_chars[2];
	for (int i = 0; i < 2; i++) value_chars[i] = string[*index + 1 - i];
	*index = *index + 2;

	unsigned short* pointer;
	pointer = (unsigned short*)value_chars;
	unsigned short value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Sread int
//
int sread_int(char* string, int* index)
{
	char value_chars[4];
	for (int i = 0; i < 4; i++) value_chars[i] = string[*index + 3 - i];
	*index = *index + 4;

	int* pointer;
	pointer = (int*)value_chars;
	int value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Sread unsigned int
//
unsigned int sread_unsigned_int(char* string, int* index)
{
	char value_chars[4];
	for (int i = 0; i < 4; i++) value_chars[i] = string[*index + 3 - i];
	*index = *index + 4;

	unsigned int* pointer;
	pointer = (unsigned int*)value_chars;
	unsigned int value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Sread int array
//
void sread_int_array(char* string, int* array, int count, int* index)
{
	char* value_chars = (char*)array;

	for (int j = 0; j < count; j++)
		for (int i = 0; i < 4; i++)
		{
			int offset = j * 4;
			value_chars[offset + i] = string[*index + offset + 3 - i];
		}

	*index = *index + count * 4;
}


//----------------------------------------------------------------------------------------
// Sread float
//
float sread_float( char* string, int* index )
{
	char value_chars[4];
	for ( int i=0; i<4; i++ ) value_chars[i] = string[*index+3-i];
	*index = *index + 4;

	float *pointer;
	pointer = (float *)value_chars;
	float value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Sread float array
//
void sread_float_array( char* string, float *array, int count, int* index )
{
	char *value_chars = (char *)array;

	for ( int j=0; j<count; j++ )
		for ( int i=0; i<4; i++ )
		{
			int offset = j*4;
			value_chars[offset+i] = string[*index+offset+3-i];
		}

	*index = *index + count*4;
}


//----------------------------------------------------------------------------------------
// Sread double
//
double sread_double( char* string, int* index )
{
	char value_chars[8];
	for ( int i=0; i<8; i++ ) value_chars[i] = string[*index+7-i];
	*index = *index + 8;

	double *pointer;
	pointer = (double *)value_chars;
	double value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Sread bool byte
//
void sread_bool_byte( char* string, char *mem_byte, int* index )
{
	mem_byte[0] = string[*index];
	*index = *index + 1;
}

	
//----------------------------------------------------------------------------------------
// Sread short bytes
//
void sread_short_bytes( char* string, char *mem_bytes, int* index )
{
	for ( int i=0; i<2; i++ ) mem_bytes[i] = string[*index+1-i];
	*index = *index + 2;
}


//----------------------------------------------------------------------------------------
// Sread int bytes
//
void sread_int_bytes( char* string, char *mem_bytes, int* index )
{
	for ( int i=0; i<4; i++ ) mem_bytes[i] = string[*index+3-i];
	*index = *index + 4;
}


//----------------------------------------------------------------------------------------
// Sread float bytes
//
void sread_float_bytes( char* string, char *mem_bytes, int* index )
{
	for ( int i=0; i<4; i++ ) mem_bytes[i] = string[*index+3-i];
	*index = *index + 4;
}


//----------------------------------------------------------------------------------------
// Sread double bytes
//
void sread_double_bytes( char* string, char *mem_bytes, int* index )
{
	for ( int i=0; i<8; i++ ) mem_bytes[i] = string[*index+7-i];
	*index = *index + 8;
}


//----------------------------------------------------------------------------------------
// Sread string
//
void sread_string( char* string, char *mem_bytes, int* index, int max_chars )
{
	int length = sread_int( string, index );
	std::cout << "sread_string() - length: " << length << std::endl;
	if ( length > max_chars-1 )
	{
		for ( int i=0; i<max_chars-1; i++ ) mem_bytes[i] = string[*index+i];
		mem_bytes[max_chars-1] = '\0';
	}
	else
	{
		for ( int i=0; i<length; i++ ) mem_bytes[i] = string[*index+i];
		mem_bytes[length] = '\0';
	}
	*index = *index + length;
}



//========================================================================================
// FILE READERS
//
// Note: size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
//
//----------------------------------------------------------------------------------------
// Read bool
//
bool read_bool( FILE* file )
{
	bool value;
	fread( &value, 1, 1, file );

	return value;
}

	
//----------------------------------------------------------------------------------------
// Read char
//
char read_char( FILE* file )
{
	char value;
	fread( &value, 1, 1, file );

	return value;
}

	
//----------------------------------------------------------------------------------------
// Read char array
//
void read_char_array( FILE* file, char *array, int count )
{
	fread( array, 1, count, file );
}


//----------------------------------------------------------------------------------------
// Read short
//
short read_short( FILE* file )
{
	char value_chars[2], rev_value_chars[2];

	fread( value_chars, 2, 1, file );
	for ( int i=0; i<2; i++ ) rev_value_chars[i] = value_chars[1-i];
	short *pointer;
	pointer = (short *)rev_value_chars;
	short value = *pointer;

	return value;
}

	
//----------------------------------------------------------------------------------------
// Read short array
//
void read_short_array( FILE* file, short *array, int count )
{
	char *value_chars = (char *)malloc( count*2 );
	char *rev_value_chars = (char *)array;

	fread( value_chars, 2, count, file );
	for ( int j=0; j<count; j++ )
		for ( int i=0; i<2; i++ )
		{
			int offset = j*2;
			rev_value_chars[offset+i] = value_chars[offset+1-i];
		}

	free( value_chars );
}


//----------------------------------------------------------------------------------------
// Read int
//
int read_int( FILE* file )
{
	char value_chars[4], rev_value_chars[4];

	fread( value_chars, 4, 1, file );
	for ( int i=0; i<4; i++ ) rev_value_chars[i] = value_chars[3-i];
	int *pointer;
	pointer = (int *)rev_value_chars;
	int value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Read int array
//
void read_int_array( FILE* file, int *array, int count )
{
	char *value_chars = (char *)malloc( count*4 );
	char *rev_value_chars = (char *)array;

	fread( value_chars, 4, count, file );
	for ( int j=0; j<count; j++ )
		for ( int i=0; i<4; i++ )
		{
			int offset = j*4;
			rev_value_chars[offset+i] = value_chars[offset+3-i];
		}

	free( value_chars );
}


//----------------------------------------------------------------------------------------
// Read float
//
float read_float( FILE* file )
{
	char value_chars[4], rev_value_chars[4];

	fread( value_chars, 4, 1, file );
	for ( int i=0; i<4; i++ ) rev_value_chars[i] = value_chars[3-i];
	float *pointer;
	pointer = (float *)rev_value_chars;
	float value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Read float array
//
void read_float_array( FILE* file, float *array, int count )
{
	char *value_chars = (char *)malloc( count*4 );
	char *rev_value_chars = (char *)array;

	fread( value_chars, 4, count, file );
	for ( int j=0; j<count; j++ )
		for ( int i=0; i<4; i++ )
		{
			int offset = j*4;
			rev_value_chars[offset+i] = value_chars[offset+3-i];
		}

	free( value_chars );
}


//----------------------------------------------------------------------------------------
// Read double
//
double read_double( FILE* file )
{
	char value_chars[8], rev_value_chars[8];

	fread( value_chars, 8, 1, file );
	for ( int i=0; i<8; i++ ) rev_value_chars[i] = value_chars[7-i];
	double *pointer;
	pointer = (double *)rev_value_chars;
	double value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Read double array
//
void read_double_array( FILE* file, double *array, int count )
{
	char *value_chars = (char *)malloc( count*8 );
	char *rev_value_chars = (char *)array;

	fread( value_chars, 8, count, file );
	for ( int j=0; j<count; j++ )
		for ( int i=0; i<8; i++ )
		{
			int offset = j*8;
			rev_value_chars[offset+i] = value_chars[offset+7-i];
		}

	free( value_chars );
}


//----------------------------------------------------------------------------------------
// Read bool byte
//
void read_bool_byte( FILE* file, char *mem_byte )
{
	fread( mem_byte, 1, 1, file );
}

	
//----------------------------------------------------------------------------------------
// Read short bytes
//
void read_short_bytes( FILE* file, char *mem_bytes )
{
	char file_bytes[2];

	fread( file_bytes, 1, 2, file );
	for ( int i=0; i<2; i++ ) mem_bytes[i] = file_bytes[1-i];
}


//----------------------------------------------------------------------------------------
// Read int bytes
//
void read_int_bytes( FILE* file, char *mem_bytes )
{
	char file_bytes[4];

	fread( file_bytes, 1, 4, file );
	for ( int i=0; i<4; i++ ) mem_bytes[i] = file_bytes[3-i];
}


//----------------------------------------------------------------------------------------
// Read float bytes
//
void read_float_bytes( FILE* file, char *mem_bytes )
{
	char file_bytes[4];

	fread( file_bytes, 1, 4, file );
	for ( int i=0; i<4; i++ ) mem_bytes[i] = file_bytes[3-i];
}


//----------------------------------------------------------------------------------------
// Read double bytes
//
void read_double_bytes( FILE* file, char *mem_bytes )
{
	char file_bytes[8];

	fread( file_bytes, 1, 8, file );
	for ( int i=0; i<8; i++ ) mem_bytes[i] = file_bytes[7-i];
}


//----------------------------------------------------------------------------------------
// Read string
//
void read_string( FILE* file, char *mem_bytes, int max_chars )
{
	int length = read_int( file );

	if ( length > max_chars-1 )
	{
		char* temp_string = (char* )malloc( length );
		fread( temp_string, 1, length, file );
		for ( int i=0; i<max_chars-1; i++ ) mem_bytes[i] = temp_string[i];
		mem_bytes[max_chars-1] = '\0';
		free( temp_string );
	}
	else
	{
		fread( mem_bytes, 1, length, file );
		mem_bytes[length] = '\0';
	}
}




