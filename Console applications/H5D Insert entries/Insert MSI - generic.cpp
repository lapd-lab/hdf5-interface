//========================================================================================
// Insert MSI - generic.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>


std::vector<int*> __i_vector;
std::vector<float*> __f_vector;
std::vector<char*> __s_vector;


//----------------------------------------------------------------------------------------
// Merge MSI Byte String
//
Json::Value merge_MSI_byte_string(
	bool is_data_array,
	Json::Value type,
	Json::Value descriptor,
	char* MSI_byte_string,
	int* str_index)
{
	Json::Value merged_value = Json::Value(Json::objectValue);

	if (type == "Cluster") {
		// "Value" looks like: 
		//     "Value": {
		//         "0": {
		//             "Name": <Name>,
		//             "Type": <Type>,
		//             "Value": <Descriptor>
		//         },
		//         "1": ...
		//     }
		//
		int member_count = descriptor.size();
		for (int i = 0; i < member_count; ++i)
		{
			Json::String index_string = std::to_string(i);
			Json::Value member_name = descriptor[index_string]["Name"];
			std::cout << "Name: " << member_name << std::endl;
			Json::Value member_type = descriptor[index_string]["Type"];
			Json::Value member_descriptor = descriptor[index_string]["Value"];

			merged_value[index_string] = Json::Value(Json::objectValue);
			bool is_data_array = false;
			merged_value[index_string]["Name"] = member_name;
			merged_value[index_string]["Type"] = member_type;
			merged_value[index_string]["Value"] = merge_MSI_byte_string(is_data_array, member_type, member_descriptor, MSI_byte_string, str_index);
		}
	}
	else if (type == "Array")
	{
		// "Value" looks like:
		//     "Value": {
		//         "Name": <Name>,
		//         "Type": <Type>,
		//         "Value": <Descriptor>
		//   }
		//
		Json::Value element_name = descriptor["Name"];
		std::cout << "Name: " << element_name << std::endl;
		Json::Value element_type = descriptor["Type"];
		Json::Value element_descriptor = descriptor["Value"];

		if (element_type == "Cluster") {
			// Special handling for array of clusters
			int cluster_count = sread_int(MSI_byte_string, str_index);
			for (int i = 0; i < cluster_count; ++i)
			{
				Json::Value element_instance = Json::Value(Json::objectValue);
				bool is_data_array = false;
				element_instance["Name"] = element_name;
				element_instance["Type"] = element_type;
				element_instance["Value"] = merge_MSI_byte_string(is_data_array, element_type, element_descriptor, MSI_byte_string, str_index);
				std::string element_instance_name = element_name.asCString();
				element_instance_name += "[" + std::to_string(i) + "]";
				std::cout << "Name: " << element_instance_name << std::endl;
				merged_value[element_instance_name] = element_instance;
			}
		}
		else
		{
			bool is_data_array = true;
			merged_value["Name"] = element_name;
			merged_value["Type"] = element_type;
			std::cout << "This is a data array of type: " << element_type << std::endl;
			merged_value["Value"] = merge_MSI_byte_string(is_data_array, element_type, element_descriptor, MSI_byte_string, str_index);
		}
	}
	else
	{
		// If the type is not "Cluster" or "Array", then this corresponds to an actual data entry in the
		// MSI byte string.  It needs to be consumed.  It may actually be an array is indicated by the is_data_array flag.
		//
		if (is_data_array) {
			// Data array
			if (type == "Single Precision")
			{
				std::cout << "Handling array of Single Precision" << std::endl;
				int data_count = sread_int(MSI_byte_string, str_index);
				std::cout << "data_count: " << data_count << std::endl;
				float* data_array = (float*)malloc(data_count * sizeof(float));
				sread_float_array(MSI_byte_string, data_array, data_count, str_index);
				__f_vector.push_back(data_array);
				merged_value = "__f_vector[" + std::to_string(__f_vector.size() - 1) + "][" + std::to_string(data_count) + "]";
			}
			else if (type == "Long")
			{
				std::cout << "Handling array of Long" << std::endl;
				int data_count = sread_int(MSI_byte_string, str_index);
				int* data_array = (int*)malloc(data_count * sizeof(float));
				sread_int_array(MSI_byte_string, data_array, data_count, str_index);
				__i_vector.push_back(data_array);
				merged_value = "__i_vector[" + std::to_string(__i_vector.size() - 1) + "][" + std::to_string(data_count) + "]";
			}
			else
			{
				std::cout << "Cannot handle array of: " << type << std::endl;
				merged_value = Json::nullValue;
			}
		}
		else {
			// Simple data element
			if (type == "Byte")
				merged_value = sread_bool(MSI_byte_string, str_index);
			else if (type == "Boolean")
				merged_value = sread_bool(MSI_byte_string, str_index);
			else if (type == "Word")
				merged_value = sread_short(MSI_byte_string, str_index);
			else if (type == "Unsigned Word")
				merged_value = sread_unsigned_short(MSI_byte_string, str_index);
			else if (type == "Long")
				merged_value = sread_int(MSI_byte_string, str_index);
			else if (type == "Unsigned Long")
				merged_value = sread_unsigned_int(MSI_byte_string, str_index);
			else if (type == "Single Precision")
				merged_value = sread_float(MSI_byte_string, str_index);
			else if (type == "Double Precision")
				merged_value = sread_double(MSI_byte_string, str_index);
			else if (type == "String")
			{
				char mem_bytes[1024];
				__s_vector.push_back(mem_bytes);
				sread_string(MSI_byte_string, mem_bytes, str_index, 1024);
				std::cout << "String: " << mem_bytes << std::endl;
				merged_value = mem_bytes;
			}
			else
				merged_value = Json::nullValue;
		}
	}

	return merged_value;
}


//----------------------------------------------------------------------------------------
// Get array length
//
int get_array_length(Json::Value value_string_json)
{
	if (!value_string_json.isString())
		return -1;

	std::string value_string = value_string_json.asCString();
	int array_length_start = value_string.find("][") + 2;
	int array_length_end = value_string.find("]", array_length_start);
	std::string sub_string = value_string.substr(array_length_start, array_length_end - array_length_start);
	if (sub_string.size() > 0)
	{
		return std::atoi(sub_string.c_str());
	}
	else
	{
		return 1;
	}
}


//----------------------------------------------------------------------------------------
// Represents int array
//
bool represents_int_array(Json::Value value_string_json)
{
	// Expecting a string value
	if (!value_string_json.isString())
		return false;

	std::string value_string = value_string_json.asCString();
	std::string sub_string = value_string.substr(0, 11);
	if (sub_string == "__i_vector[")
		return true;
	else
		return false;
}


//----------------------------------------------------------------------------------------
// Represents float array
//
bool represents_float_array(Json::Value value_string_json)
{
	// Expecting a string value
	if (!value_string_json.isString())
		return false;

	std::string value_string = value_string_json.asCString();
	std::string sub_string = value_string.substr(0, 11);
	if (sub_string == "__f_vector[")
		return true;
	else
		return false;
}


//----------------------------------------------------------------------------------------
// Get float array
//
int* get_int_array(Json::Value value_string_json)
{
	// Expecting a string value
	if (!value_string_json.isString())
		return false;

	std::string value_string = value_string_json.asCString();
	std::string sub_string = value_string.substr(0, 11);
	if (sub_string == "__i_vector[")
	{
		int vector_index_end = value_string.find("]");
		std::string sub_string = value_string.substr(11, vector_index_end - 11);
		int vector_index = std::atoi(sub_string.c_str());
		return __i_vector[vector_index];
	}
	else return nullptr;
}


//----------------------------------------------------------------------------------------
// Get float array
//
float* get_float_array(Json::Value value_string_json)
{
	// Expecting a string value
	if (!value_string_json.isString())
		return false;

	std::string value_string = value_string_json.asCString();
	std::string sub_string = value_string.substr(0, 11);
	if (sub_string == "__f_vector[")
	{
		int vector_index_end = value_string.find("]");
		std::string sub_string = value_string.substr(11, vector_index_end - 11);
		int vector_index = std::atoi(sub_string.c_str());
		return __f_vector[vector_index];
	}
	else return nullptr;
}


//----------------------------------------------------------------------------------------
// Extract Summary JSON
//
Json::Value extract_summary_json(Json::Value in_summary_json, Json::Value& merged_MSI_value)
{
	Json::Value out_summary_json = in_summary_json;
	// Loop through merged_MSI_value (assumed to be a Cluster) and extract the simple values, appending after values already present

	int merged_MSI_value_count = merged_MSI_value.size();
	int out_summary_index = in_summary_json.size();
	for (int i = 0; i < merged_MSI_value_count; ++i)
	{
		Json::String index_string = std::to_string(i);
		Json::Value member_name = merged_MSI_value[index_string]["Name"];
		Json::Value member_type = merged_MSI_value[index_string]["Type"];
		Json::Value member_value = merged_MSI_value[index_string]["Value"];
		if ((member_type != "Cluster") && (member_type != "Array"))
		{
			Json::String summary_index_string = std::to_string(out_summary_index);
			out_summary_json[summary_index_string]["Name"] = member_name;
			out_summary_json[summary_index_string]["Type"] = member_type;
			out_summary_json[summary_index_string]["Value"] = member_value;
			out_summary_index++;
			merged_MSI_value.removeMember(index_string);
		}
	}
	return out_summary_json;
}


//----------------------------------------------------------------------------------------
// Get MSI HDF5 JSON
//
Json::Value get_MSI_HDF5_json(Json::Value summary_json, Json::Value merged_MSI_json)
{
	Json::Value merged_MSI_name = merged_MSI_json["Name"];
	Json::Value merged_MSI_type = merged_MSI_json["Type"];
	Json::Value merged_MSI_value = merged_MSI_json["Value"];

	Json::Value MSI_HDF5_json(Json::objectValue);
	MSI_HDF5_json["Name"] = merged_MSI_name;
	if (merged_MSI_type == "Cluster")
	{
		MSI_HDF5_json["Type"] = "HDF5 Group";
		MSI_HDF5_json["Type: Group Type"] = "from Cluster";
		MSI_HDF5_json["Value"] = Json::Value(Json::objectValue);
		summary_json = extract_summary_json(summary_json, merged_MSI_value);

		Json::Value::Members merged_MSI_member_names = merged_MSI_value.getMemberNames();
		int merged_MSI_member_count = merged_MSI_member_names.size();
		for (int i = 0; i < merged_MSI_member_count; ++i)
		{
			Json::Value merged_MSI_member = merged_MSI_value[merged_MSI_member_names[i]];
			Json::String index_string = std::to_string(i);
			MSI_HDF5_json["Value"][index_string] = get_MSI_HDF5_json(summary_json, merged_MSI_member);
		}
		Json::Value summary_HDF5(Json::objectValue);
		std::string summary_name = merged_MSI_name.asCString();
		summary_name += " summary";
		summary_HDF5["Name"] = summary_name;
		summary_HDF5["Type"] = "HDF5 Dataset";
		summary_HDF5["Type: Data Type"] = "Compound";
		summary_HDF5["Type: Number of Dimensions"] = 1;
		summary_HDF5["Value"] = summary_json;
		Json::String summary_index_string = std::to_string(merged_MSI_member_count);
		MSI_HDF5_json["Value"][summary_index_string] = summary_HDF5;
	}
	else if (merged_MSI_type == "Array")
	{
		// Array handling
		if (merged_MSI_value.isMember("Name"))
		{
			// This is an array of simple values
			MSI_HDF5_json["Type"] = "HDF5 Dataset";
			MSI_HDF5_json["Type: Number of Dimensions"] = 2;
			if (represents_int_array(merged_MSI_value["Value"]))
			{
				MSI_HDF5_json["Type: Data Type"] = "32-bit integer";
				MSI_HDF5_json["Value"] = merged_MSI_value["Value"];
			}
			else if (represents_float_array(merged_MSI_value["Value"]))
			{
				MSI_HDF5_json["Type: Data Type"] = "32-bit floating-point";
				MSI_HDF5_json["Value"] = merged_MSI_value["Value"];
			}
			else
			{
				MSI_HDF5_json["Type: Data Type"] = "Unknown";
				MSI_HDF5_json["Value"] = Json::nullValue;
			}
		}
		else
		{
			// This is an array of clusters - handled like with clusters above, just without extracting summary
			MSI_HDF5_json["Type"] = "HDF5 Group";
			MSI_HDF5_json["Type: Group Type"] = "from Array";
			MSI_HDF5_json["Value"] = Json::Value(Json::objectValue);
			Json::Value::Members merged_MSI_member_names = merged_MSI_value.getMemberNames();
			int merged_MSI_member_count = merged_MSI_member_names.size();
			for (int i = 0; i < merged_MSI_member_count; ++i)
			{
				Json::Value merged_MSI_member = merged_MSI_value[merged_MSI_member_names[i]];
				MSI_HDF5_json["Value"][merged_MSI_member_names[i]] = get_MSI_HDF5_json(summary_json, merged_MSI_member);

				// With an array, we can make our the HDF5 update easier later by adjusting the name of each array element to reflect the index
				MSI_HDF5_json["Value"][merged_MSI_member_names[i]]["Name"] = merged_MSI_member_names[i];
			}
		}
	}
	else
	{
		// This shouldn't happen
		MSI_HDF5_json["Type"] = "Unknown";
		MSI_HDF5_json["Value"] = Json::nullValue;
	}
	return MSI_HDF5_json;
}


//----------------------------------------------------------------------------------------
// Update HDF5 Group
//
void update_HDF5_group(hid_t loc_id, Json::Value MSI_HDF5_json, int* MSI_index, int* error_index)
{
	Json::Value name = MSI_HDF5_json["Name"];

	// Create group if it doesn't exist
	hid_t group_id = H5Gopen2(loc_id, name.asCString(), H5P_DEFAULT);
	if (group_id < 0)
	{
		group_id = H5Gcreate2(loc_id, name.asCString(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if (group_id < 0)
		{
			// insert error entry
			return;
		}
	}

	// Update the group
	Json::Value group_type = MSI_HDF5_json["Type: Group Type"];
	if (group_type == "from Cluster")
	{
		// Group from Cluster handling
		Json::Value group_members_json = MSI_HDF5_json["Value"];
		int member_count = group_members_json.size();
		for (int i = 0; i < member_count; ++i)
		{
			std::string index_string = std::to_string(i);
			Json::Value group_member_json = group_members_json[index_string];
			update_HDF5_object(group_id, group_member_json, MSI_index, error_index);
		}
	}
	else if (group_type == "from Array")
	{
		// Group from Array handling
		Json::Value::Members member_names = MSI_HDF5_json["Value"].getMemberNames();
		int member_count = member_names.size();
		for (int i = 0; i < member_count; ++i) {
			Json::Value array_member_json = MSI_HDF5_json["Value"][member_names[i]];
			update_HDF5_object(group_id, array_member_json, MSI_index, error_index);
		}
	}
	else
	{
		// Unrecognized group type: insert error entry
	}

	H5Gclose(group_id);
}


//----------------------------------------------------------------------------------------
// Update HDF5 1D Dataset
//
void update_HDF5_1D_dataset(hid_t loc_id, Json::Value MSI_HDF5_json, int* MSI_index, int* error_index)
{
	// Create err and IDs
	herr_t err = 0;
	hid_t file_space_id = -1;
	hid_t mem_space_id = -1;
	hid_t mem_type_id = -1;
	hid_t plist_id = -1;

	// File dataspace definition
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { (*MSI_index) + 1 };
	hsize_t file_maxdims[1] = { 4294967295 };

	// Hyperslab definition
	hsize_t start[1] = { (*MSI_index) };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };

	// Memory dataspace definition
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { 1 };
	hsize_t mem_maxdims[1] = { 1 };


	// *****************************
	// *** Create/Extend Dataset ***
	//
	Json::Value name = MSI_HDF5_json["Name"];
	hid_t dataset_id = H5Dopen2(loc_id, name.asCString(), H5P_DEFAULT);
	if (dataset_id < 0)
	{
		// Create file dataspace
		file_space_id = H5Screate_simple(file_rank, file_dims, file_maxdims);
		if (file_space_id < 0) err = -1;
	}
	else
	{
		// Extend file dataspace
		file_space_id = H5Dget_space(dataset_id);
		if (file_space_id < 0) err = -1;
		if (err == 0) err = H5Sset_extent_simple(file_space_id, file_rank, file_dims, file_maxdims);
	}

	// Create hyperslab
	if (err == 0) err = H5Sselect_hyperslab(file_space_id, H5S_SELECT_SET, start, stride, count, block);

	// Create memory dataspace
	if (err == 0) mem_space_id = H5Screate_simple(mem_rank, mem_dims, mem_maxdims);
	if (mem_space_id < 0) err = -1;

	// Create memory datatype generically
	//
	hid_t char_field_id = H5Tcopy(H5T_NATIVE_CHAR);
	hid_t short_field_id = H5Tcopy(H5T_NATIVE_SHORT);
	hid_t unsigned_short_field_id = H5Tcopy(H5T_NATIVE_USHORT);
	hid_t int_field_id = H5Tcopy(H5T_NATIVE_INT);
	hid_t unsigned_int_field_id = H5Tcopy(H5T_NATIVE_UINT);
	hid_t float_field_id = H5Tcopy(H5T_NATIVE_FLOAT);
	hid_t double_field_id = H5Tcopy(H5T_NATIVE_DOUBLE);
	char* compound_data_buffer;
	int member_count;
	std::vector<int> member_sizes;
	std::vector<hid_t> member_field_ids;
	std::vector<char*> member_data_byte_strings;

	if (err == 0)
	{
		Json::Value compound_data_descriptor = MSI_HDF5_json["Value"];
		member_count = compound_data_descriptor.size();
		for (int i = 0; i < member_count; ++i)
		{
			std::string index_string = std::to_string(i);
			Json::Value member = compound_data_descriptor[index_string];
			Json::Value type = member["Type"];
			Json::Value value = member["Value"];
			if (type == "Byte")
			{
				member_sizes.push_back(1); 
				member_field_ids.push_back(char_field_id);
				char* member_data_ptr = (char*)malloc(sizeof(char));
				char* member_data_bytes;
				char value_char = (char)value.asInt();
				*member_data_ptr = value_char;
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "Boolean")
			{
				member_sizes.push_back(1);
				member_field_ids.push_back(char_field_id);
				bool* member_data_ptr = (bool*)malloc(sizeof(bool));
				char* member_data_bytes;
				*member_data_ptr = value.asBool();
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "Word")
			{
				member_sizes.push_back(2);
				member_field_ids.push_back(short_field_id);
				short* member_data_ptr = (short*)malloc(sizeof(short));
				char* member_data_bytes;
				short value_short = (short)value.asInt();
				*member_data_ptr = value_short;
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "Unsigned Word")
			{
				member_sizes.push_back(2);
				member_field_ids.push_back(unsigned_short_field_id);
				unsigned short* member_data_ptr = (unsigned short*)malloc(sizeof(unsigned short));
				char* member_data_bytes;
				unsigned short value_short = (unsigned short)value.asInt();
				*member_data_ptr = value_short;
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "Long")
			{
				member_sizes.push_back(4);
				member_field_ids.push_back(int_field_id);
				int* member_data_ptr = (int*)malloc(sizeof(int));
				char* member_data_bytes;
				*member_data_ptr = value.asInt();
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "Unsigned Long")
			{
				member_sizes.push_back(4);
				member_field_ids.push_back(unsigned_int_field_id);
				unsigned int* member_data_ptr = (unsigned int*)malloc(sizeof(unsigned int));
				char* member_data_bytes;
				*member_data_ptr = (unsigned int)value.asInt();
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "Single Precision")
			{
				std::cout << "Handling Single Precision for HDF5 compound dataset" << std::endl;
				member_sizes.push_back(4);
				member_field_ids.push_back(float_field_id);
				float* member_data_ptr = (float*)malloc(sizeof(float));
				char* member_data_bytes;
				*member_data_ptr = value.asFloat();
				std::cout << "*member_data_ptr: " << *member_data_ptr << std::endl;
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "Double Precision")
			{
				member_sizes.push_back(8);
				member_field_ids.push_back(double_field_id);
				double* member_data_ptr = (double*)malloc(sizeof(double));
				char* member_data_bytes;
				*member_data_ptr = value.asDouble();
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else if (type == "String")
			{
				std::cout << "Handling String for HDF5 compound dataset" << std::endl;
				int member_size = strlen(value.asCString()) + 1;
				std::cout << "member_size: " << member_size << std::endl;
				member_sizes.push_back(member_size);
				hid_t string_field_id = H5Tcopy(H5T_C_S1);
				H5Tset_size(string_field_id, member_size);
				member_field_ids.push_back(string_field_id);
				char* member_data_ptr = (char*)malloc(member_size);
				char* member_data_bytes;
				for (int i = 0; i < member_size; ++i)
				{
					member_data_ptr[i] = value.asCString()[i];
				}
				member_data_bytes = member_data_ptr;
				std::cout << "member_data_bytes: " << member_data_bytes << std::endl;
				member_data_byte_strings.push_back(member_data_bytes);
			}
			else
			{
				member_sizes.push_back(0);
				member_field_ids.push_back(0);
				bool* member_data_ptr = (bool*)malloc(sizeof(bool));
				char* member_data_bytes;
				*member_data_ptr = 0;
				member_data_bytes = (char*)member_data_ptr;
				member_data_byte_strings.push_back(member_data_bytes);
			}
		}

		int compound_data_size = 0;
		for (int i = 0; i < member_sizes.size(); ++i) compound_data_size += member_sizes[i];
		std::cout << "compound_data_size: " << compound_data_size << std::endl;
		mem_type_id = H5Tcreate(H5T_COMPOUND, compound_data_size);
		if (mem_type_id < 0) err = -1;

		compound_data_buffer = (char*)malloc(compound_data_size);
		int buffer_index = 0;
		std::cout << "buffer_index starts at: " << buffer_index << std::endl;
		for (int i = 0; i < member_count; ++i)
		{
			std::string index_string = std::to_string(i);
			Json::Value member = compound_data_descriptor[index_string];
			Json::Value name = member["Name"];
			std::cout << "H5Tinsert() for: " << name << std::endl;
			std::cout << "field id size: " << H5Tget_size(member_field_ids[i]) << std::endl;
			std::cout << "Before, err: " << err << std::endl;
			if (err == 0) err = H5Tinsert(mem_type_id, name.asCString(), buffer_index, member_field_ids[i]);
			std::cout << "After, err: " << err << std::endl;
			for (int j = 0; j < member_sizes[i]; j++) compound_data_buffer[buffer_index + j] = member_data_byte_strings[i][j];
			buffer_index += member_sizes[i];
			std::cout << "buffer_index now: " << buffer_index << std::endl;
		}
	}
	H5Tclose(char_field_id);
	H5Tclose(short_field_id);
	H5Tclose(unsigned_short_field_id);
	H5Tclose(int_field_id);
	H5Tclose(unsigned_int_field_id);
	H5Tclose(float_field_id);
	H5Tclose(double_field_id);
	for (int i = 0; i < member_field_ids.size(); ++i)
	{
		H5Tclose(member_field_ids[i]);
	}

	if (dataset_id < 0)
	{
		// Dataset creation property list
		//
		if (err == 0) plist_id = H5Pcreate(H5P_DATASET_CREATE);
		if (plist_id < 0) err = -1;

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		if (err == 0)
		{
			H5Pset_chunk(plist_id, file_rank, file_chunk);
			H5Pset_deflate(plist_id, compression);
		}

		// Everything is set up so now create the 2D dataset
		//
		if (err == 0)
		{
			Json::Value name = MSI_HDF5_json["Name"];
			dataset_id = H5Dcreate2(loc_id, name.asCString(), mem_type_id, file_space_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);
			if (dataset_id < 0) err = -1;
		}

		H5Pclose(plist_id);
	}
	else
	{
		// Extend the dataset
		//
		if (err == 0) err = H5Dextend(dataset_id, file_dims);
	}


	// *******************************************
	// *** Write the 1D array into the dataset ***
	//
	if (err == 0)
	{
		fprintf(stderr, "About to H5Dwrite()\n");  fflush(stdout);
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, compound_data_buffer);
		std::cout << "err: " << err << std::endl;
	}


	// *****************************************
	// *** Error handling and memory freeing ***
	//
	if (err < 0)
	{
		//write_hdf5_error(efn, shot_number);
		//insert_hdf5_error(file_id, error_index, shot_number);
	}

	H5Tclose(mem_type_id);
	H5Sclose(mem_space_id);
	H5Sclose(file_space_id);
	H5Dclose(dataset_id);
	H5Pclose(plist_id);
	free(compound_data_buffer);

	for (int i = 0; i < member_count; ++i)
	{
		free(member_data_byte_strings[i]);
	}

}


//----------------------------------------------------------------------------------------
// Update HDF5 2D Dataset
//
void update_HDF5_2D_dataset(hid_t loc_id, Json::Value MSI_HDF5_json, int* MSI_index, int* error_index)
{
	// Create err and IDs
	herr_t err = 0;
	hid_t file_space_id = -1;
	hid_t mem_space_id = -1;
	hid_t mem_type_id = -1;
	hid_t plist_id = -1;

	// File dataspace definition
	//
	int file_rank = 2;
	Json::Value data_reference = MSI_HDF5_json["Value"];
	int data_length = get_array_length(data_reference);
	hsize_t file_dims[2] = { (*MSI_index) + 1, data_length };
	hsize_t file_maxdims[2] = { 4294967295, data_length };

	// Hyperslab definition
	//
	hsize_t start[2] = { (*MSI_index), 0 };
	hsize_t stride[2] = { 1, 1 };
	hsize_t count[2] = { 1, 1 };
	hsize_t block[2] = { 1, data_length };

	// Memory dataspace definition
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { data_length };
	hsize_t mem_maxdims[1] = { data_length };


	// *****************************
	// *** Create/Extend Dataset ***
	//
	Json::Value name = MSI_HDF5_json["Name"];
	hid_t dataset_id = H5Dopen2(loc_id, name.asCString(), H5P_DEFAULT);
	if (dataset_id < 0)
	{
		// Create file dataspace
		file_space_id = H5Screate_simple(file_rank, file_dims, file_maxdims);
		if (file_space_id < 0) err = -1;
	}
	else
	{
		// Extend file dataspace
		file_space_id = H5Dget_space(dataset_id);
		if (file_space_id < 0) err = -1;
		if (err == 0) err = H5Sset_extent_simple(file_space_id, file_rank, file_dims, file_maxdims);
	}

	// Create hyperslab
	if (err == 0) err = H5Sselect_hyperslab(file_space_id, H5S_SELECT_SET, start, stride, count, block);

	// Create memory dataspace
	if (err == 0) mem_space_id = H5Screate_simple(mem_rank, mem_dims, mem_maxdims);
	if (mem_space_id < 0) err = -1;

	// Create memory datatype
	//
	Json::Value data_type = MSI_HDF5_json["Type: Data Type"];
	if (err == 0)
	{
	if (data_type == "32-bit integer") mem_type_id = H5Tcopy(H5T_NATIVE_INT);
	else if (data_type == "32-bit floating-point") mem_type_id = H5Tcopy(H5T_NATIVE_FLOAT);
	}
	if (mem_type_id < 0) err = -1;

	if (dataset_id < 0)
	{
		// Create the dataset
		//
		// Dataset creation property list
		if (err == 0) plist_id = H5Pcreate(H5P_DATASET_CREATE);
		if (plist_id < 0) err = -1;

		hsize_t file_chunk[2] = { 1, data_length };
		if (err == 0)
		{
			int compression = 4;
			H5Pset_chunk(plist_id, file_rank, file_chunk);
			H5Pset_deflate(plist_id, compression);
		}

		// Everything is set up so now create the 2D dataset
		//
		Json::Value name = MSI_HDF5_json["Name"];
		if (err == 0) dataset_id = H5Dcreate2(loc_id, name.asCString(), mem_type_id, file_space_id, H5P_DEFAULT, plist_id, H5P_DEFAULT);
		if (dataset_id < 0) err = -1;
	}
	else
	{
		// Extend the dataset
		if (err == 0) err = H5Dextend(dataset_id, file_dims);
	}


	// *******************************************
	// *** Write the 2D array into the dataset ***
	//
	if (err == 0)
	{
		fprintf(stderr, "About to H5Dwrite()\n");  fflush(stdout);
		if (data_type == "32-bit integer")
		{
			int* int_array = get_int_array(data_reference);
			for (int i = 0; i < data_length; ++i)
			{
				std::cout << name << ": int_array[" << i << "]: " << int_array[i] << std::endl;
			}
			err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, int_array);
			std::cout << "err: " << err << std::endl;
		}
		else if (data_type == "32-bit floating-point")
		{
			float* float_array = get_float_array(data_reference);
			for (int i = 0; i < data_length; ++i)
			{
				std::cout << name << ": float_array[" << i << "]: " << float_array[i] << std::endl;
			}
			err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, float_array);
			std::cout << "err: " << err << std::endl;
		}
		else err = -1;
	}


	// *****************************************
	// *** Error handling and memory freeing ***
	//
	if (err < 0)
	{
		//write_hdf5_error(efn, shot_number);
		//insert_hdf5_error(file_id, error_index, shot_number);
	}
	
	H5Tclose(mem_type_id);
	H5Sclose(mem_space_id);
	H5Sclose(file_space_id);
	H5Dclose(dataset_id);
	H5Pclose(plist_id);
}


//----------------------------------------------------------------------------------------
// Update HDF5 Object
//
void update_HDF5_object(hid_t loc_id, Json::Value MSI_HDF5_json, int* MSI_index, int *error_index)
{
	Json::Value type = MSI_HDF5_json["Type"];

	// **********************
	// *** Group handling ***
	//
	if (type == "HDF5 Group") update_HDF5_group(loc_id, MSI_HDF5_json, MSI_index, error_index);

	// ************************
	// *** Dataset handling ***
	//
	else if (type == "HDF5 Dataset")
	{
		int dimension_count = MSI_HDF5_json["Type: Number of Dimensions"].asInt();
		if (dimension_count == 1) update_HDF5_1D_dataset(loc_id, MSI_HDF5_json, MSI_index, error_index);
		else if (dimension_count == 2) update_HDF5_2D_dataset(loc_id, MSI_HDF5_json, MSI_index, error_index);
		else
		{
			// Unhandled dimension count: insert error entry
		}
	}

	// *************************
	// *** Unrecognized type ***
	//
	else
	{
		// Unrecognized type: insert error entry
	}
}


//----------------------------------------------------------------------------------------
// Extend/Create summary dataset
//
herr_t extend_create_summary_dataset_POC(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	herr_t err = 0;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen2( group_id, dataset, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;
	}


	// File dataspace
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { index+1 };
	hsize_t file_maxdims[1] = { 4294967295 };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) return -1;
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) return -1;
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) return -1;
	}

	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) return -1;


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { 1 };
	hsize_t mem_maxdims[1] = { 1 };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) return -1;


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	hid_t char_field_id = H5Tcopy( H5T_NATIVE_CHAR );
	hid_t int_field_id = H5Tcopy( H5T_NATIVE_INT );
	hid_t float_field_id = H5Tcopy( H5T_NATIVE_FLOAT );
	hid_t double_field_id = H5Tcopy( H5T_NATIVE_DOUBLE );
	if ( strcmp(dataset, "Discharge summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 25 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Pulse length", 13, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak current", 17, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Bank voltage", 21, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Heater summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 25 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Heater current", 13, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Heater voltage", 17, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Heater temperature", 21, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Gas pressure summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 22 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Ion gauge data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "RGA data valid", 13, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Fill pressure", 14, float_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak AMU", 18, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Magnetic field summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 17 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak magnetic field", 13, float_field_id );  if ( err < 0 ) return -1;
	}

	if ( strcmp(dataset, "Interferometer summary list") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 17 );
		if ( *mem_type_id < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) return -1;
		err = H5Tinsert( *mem_type_id, "Peak density", 13, float_field_id );  if ( err < 0 ) return -1;
	}

	H5Tclose( char_field_id );  H5Tclose( int_field_id );  H5Tclose( float_field_id );  H5Tclose( double_field_id );


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) return -1;
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) return -1;

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate2( group_id, dataset, *mem_type_id, *file_space_id, H5P_DEFAULT, plist_id, H5P_DEFAULT );
		if ( *dataset_id < 0 ) return -1;

		H5Pclose( plist_id );
	}

	return 0;
}


//----------------------------------------------------------------------------------------
// Insert MSI generic
//
herr_t insert_MSI_generic(
	std::ifstream& descriptor_file,
	hid_t file_id,
	char* MSI_data_run_folder,
	char* efn,
	int shot_number,
	int* MSI_index,
	int* error_index)
{
	herr_t err = 0;

	// *************************************************************************
	// *** Open the top_level_descriptor file and read it into a JSON object ***
	//
	//		[ "Housekeeper_MSI", <another MSI>, ... ]
	//
	Json::Value top_level_descriptor;
	descriptor_file >> top_level_descriptor;
	fprintf(stdout, "top_level_descriptor is an array: %d\n", top_level_descriptor.isArray()); fflush(stdout);
	if (!top_level_descriptor.isArray()) err = -1;

	// Loop through the MSI's and process
	//
	int MSI_count = top_level_descriptor.size();
	for (int i = 0; i < MSI_count; ++i) {
		std::cout << top_level_descriptor[i] << std::endl;

		// *******************************************************************************
		// *** Open the MSI byte string descriptor file and read it into a JSON object ***
		//		{
		//          "Name" : "Housekeeper MSI",
		//          	"Type" : "Cluster",
		//          	"Value" : {
		//          		"0" : {
		//          			"Name": "Timestamp",
		//          			"Type" : "Double Precision",
		//          			"Value" : null
		//          		},
		//					...
		//
		char MSI_descriptor_filename[2048];
		sprintf_s(MSI_descriptor_filename, 2048, "%s/%s_descriptor.%06d.json", MSI_data_run_folder, top_level_descriptor[i].asCString(), shot_number);
		fprintf(stdout, "About to open: %s\n", MSI_descriptor_filename); fflush(stdout);
		std::ifstream MSI_descriptor_file(MSI_descriptor_filename);

		Json::Value MSI_byte_string_descriptor;   // 'MSI_byte_string_descriptor' will be the MSI root object after parsing.
		MSI_descriptor_file >> MSI_byte_string_descriptor;
		fprintf(stdout, "MSI_byte_string_descriptor is a _dict: %d\n", MSI_byte_string_descriptor.isObject()); fflush(stdout);
		if (!MSI_byte_string_descriptor.isObject()) err = -1;

		Json::Value MSI_root_name;
		Json::Value MSI_root_type;
		Json::Value MSI_root_value;

		if (err == 0)
		{
			MSI_root_name = MSI_byte_string_descriptor["Name"];
			if (MSI_root_name != top_level_descriptor[i]) err = -1;
			MSI_root_type = MSI_byte_string_descriptor["Type"];
			MSI_root_value = MSI_byte_string_descriptor["Value"];
			std::cout << "root_name is: " << MSI_root_name << std::endl;
			std::cout << "root_type is: " << MSI_root_type << std::endl;
			if (MSI_root_type != "Cluster") err = -1;
			std::cout << "root_value.isObject(): " << MSI_root_value.isObject() << std::endl;
			if (!MSI_root_value.isObject()) err = -1;
		}

		// ***************************************************************
		// *** Open the MSI byte string file and read it into a string ***
		//
		FILE* MSI_file;
		char* MSI_byte_string;
		if (err == 0)
		{
			char MSI_filename[2048];
			sprintf_s(MSI_filename, 2048, "%s/%s.%06d.dat", MSI_data_run_folder, MSI_root_name.asCString(), shot_number);
			fprintf(stderr, "About to open %s\n", MSI_filename);  fflush(stdout);
			errno_t fopen_err = fopen_s(&MSI_file, MSI_filename, "rb");  fprintf(stderr, "fopen_err: %d\n", fopen_err);  fflush(stdout);  if (fopen_err) return -1;
			int MSI_byte_string_length = read_int(MSI_file);
			fprintf(stderr, "MSI byte string length: %d\n", MSI_byte_string_length);  fflush(stdout);

			MSI_byte_string = (char*)malloc(MSI_byte_string_length);
			// size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
			size_t nread = fread(MSI_byte_string, 1, MSI_byte_string_length, MSI_file);
			std::cout << "nread: " << nread << std::endl;

			if (nread != MSI_byte_string_length) err = -1;
		}

		// *********************************************************************
		// *** Merge the values from the MSI byte string into the descriptor ***
		//		{
		//			"Name" : "Housekeeper_MSI",
		//			"Type" : "Cluster",
		//			"Value" : {
		//				"0" : {
		//					"Name" : "Timestamp",
		//					"Type" : "Double Precision",
		//					"Value" : 31198876.539999999
		//				},
		//				...
		//
		// Note that data arrays are held in global lists and just referred to in the merged MSI JSON.
		//
		int str_index = 0;
		Json::Value merged_MSI_json(Json::objectValue);
		if (err == 0)
		{
			// Merge MSI descriptor with MSI byte string -> merged MSI
			bool is_array_element = false;
			merged_MSI_json["Name"] = MSI_root_name;
			merged_MSI_json["Type"] = MSI_root_type;
			merged_MSI_json["Value"] = merge_MSI_byte_string(is_array_element, MSI_root_type, MSI_root_value, MSI_byte_string, &str_index);

			Json::Value merged_MSI_root_value = merged_MSI_json["Value"];

			char merged_MSI_filename[2048];
			sprintf_s(merged_MSI_filename, 2048, "%s/%s_merged_MSI.%06d.json", MSI_data_run_folder, MSI_root_name, shot_number);
			fprintf(stderr, "Opening %s as ofstream\n", merged_MSI_filename);  fflush(stdout);
			std::ofstream merged_MSI_file(merged_MSI_filename);
			merged_MSI_file << merged_MSI_json;

			// **************************************************
			// *** Translate merged MSI JSON to MSI HDF5 JSON ***
			//		{
			//			"Name" : "Housekeeper_MSI",
			//			"Type" : "HDF5 Group",
			//			"Type: Group Type" : "from Cluster",
			//			"Value" : {
			//				"0" : {
			//					"Name" : "Discharge",
			//					"Type" : "HDF5 Group",
			//					"Type: Group Type" : "from Cluster",
			//					"Value" : {
			//						"0" : {
			//							"Name" : "Discharge current",
			//							"Type" : "HDF5 Dataset",
			//							"Type: Data Type" : "32-bit floating-point",
			//							"Type: Number of Dimensions" : 2,
			//							"Value" : "__f_vector[0]"
			//						},
			//						...
			//
			// Note that, in this translation, simple data items are gathered into HDF5 Compound Datasets suffixed "summary"
			//
			Json::Value summary_json = Json::Value(Json::objectValue);
			summary_json["0"] = Json::Value(Json::objectValue);
			summary_json["0"]["Name"] = "Shot Number";
			summary_json["0"]["Type"] = "Long";
			summary_json["0"]["Value"] = shot_number;

			Json::Value MSI_HDF5_json = get_MSI_HDF5_json(summary_json, merged_MSI_json);

			char MSI_HDF5_filename[2048];
			sprintf_s(MSI_HDF5_filename, 2048, "%s/%s_MSI_HDF5.%06d.json", MSI_data_run_folder, MSI_root_name, shot_number);
			fprintf(stderr, "Opening %s as ofstream\n", MSI_HDF5_filename);  fflush(stdout);
			std::ofstream MSI_HDF5_file(MSI_HDF5_filename);
			MSI_HDF5_file << MSI_HDF5_json;

			// ***********************************************
			// *** Update HDF5 file from the MSI HDF5 JSON ***
			//
			std::cout << "About to update HDF5 file with: " << MSI_HDF5_json["Name"] << std::endl;
			update_HDF5_object(file_id, MSI_HDF5_json, MSI_index, error_index);
		}
	}

	for (int i = 0; i < __i_vector.size(); ++i)
	{
		free(__i_vector[i]);
	}
	__i_vector.clear();

	for (int i = 0; i < __f_vector.size(); ++i)
	{
		free(__f_vector[i]);
	}
	__f_vector.clear();

	__s_vector.clear();

	*MSI_index += 1;
	return err;
}


//----------------------------------------------------------------------------------------
// Insert MSI legacy
//
herr_t insert_MSI_legacy(
	hid_t file_id,
	char* data_run_folder,
	char* efn,
	int shot_number,
	int* MSI_index,
	int* error_index)
{
	// Deal with shot number and index
	//
	int* shot_number_ptr = (int*)malloc(sizeof(int));
	char* shot_number_bytes;
	*shot_number_ptr = shot_number;
	shot_number_bytes = (char*)shot_number_ptr;
	int index = *MSI_index;


	// Open the MSI string file and read (for the Housekeeper MSI only)
	//
	herr_t err = 0;
	FILE* MSI_file;
	char* housekeeper_MSI_string;
	if (err == 0)
	{
		char MSI_filename[2048];
		sprintf_s(MSI_filename, 2048, "%s/MSI/MSI.%06d.dat", data_run_folder, shot_number);
		fprintf(stderr, "About to open %s\n", MSI_filename);  fflush(stdout);
		errno_t fopen_err = fopen_s(&MSI_file, MSI_filename, "rb");  fprintf(stderr, "fopen_err: %d\n", fopen_err);  fflush(stdout);  if (fopen_err) return -1;
		int housekeeper_MSI_length = read_int(MSI_file);
		fprintf(stderr, "Housekeeper MSI length: %d\n", housekeeper_MSI_length);  fflush(stdout);

		housekeeper_MSI_string = (char*)malloc(housekeeper_MSI_length);
		// size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
		size_t nread = fread(housekeeper_MSI_string, 1, housekeeper_MSI_length, MSI_file);

		if (nread != housekeeper_MSI_length) err = -1;
	}


	// Read timestamp
	//
	int str_index = 0;
	double timestamp_value;
	if (err == 0)
		timestamp_value = sread_double(housekeeper_MSI_string, &str_index);
	double* timestamp = (double*)malloc(sizeof(double));
	char* timestamp_bytes;
	*timestamp = timestamp_value;
	timestamp_bytes = (char*)timestamp;


	// Read discharge
	//
	struct dischargeStruct discharge;
	if (err == 0)
	{
		for (int i = 0; i < 4; i++) discharge.summary_buffer[0 + i] = shot_number_bytes[i];
		for (int i = 0; i < 8; i++) discharge.summary_buffer[4 + i] = timestamp_bytes[i];

		sread_bool_byte(housekeeper_MSI_string, &(discharge.summary_buffer[12]), &str_index);  // discharge.data_valid

		discharge.current_count = sread_int(housekeeper_MSI_string, &str_index);
		discharge.current = (float*)malloc(discharge.current_count * sizeof(float));
		sread_float_array(housekeeper_MSI_string, discharge.current, discharge.current_count, &str_index);

		discharge.voltage_count = sread_int(housekeeper_MSI_string, &str_index);
		discharge.cathode_anode_voltage = (float*)malloc(discharge.voltage_count * sizeof(float));
		sread_float_array(housekeeper_MSI_string, discharge.cathode_anode_voltage, discharge.voltage_count, &str_index);

		sread_float_bytes(housekeeper_MSI_string, &(discharge.summary_buffer[13]), &str_index);  // discharge.pulse_length 
		sread_float_bytes(housekeeper_MSI_string, &(discharge.summary_buffer[17]), &str_index);  // discharge.peak_current
		sread_float_bytes(housekeeper_MSI_string, &(discharge.summary_buffer[21]), &str_index);  // discharge.bank_voltage
	}
	else
	{
		discharge.current = (float*)malloc(1024 * sizeof(float));
		discharge.cathode_anode_voltage = (float*)malloc(1024 * sizeof(float));
	}


	// Read heater
	//
	struct heaterStruct heater;
	if (err == 0)
	{
		for (int i = 0; i < 4; i++) heater.summary_buffer[0 + i] = shot_number_bytes[i];
		for (int i = 0; i < 8; i++) heater.summary_buffer[4 + i] = timestamp_bytes[i];

		sread_bool_byte(housekeeper_MSI_string, &(heater.summary_buffer[12]), &str_index);  // heater.data_valid
		sread_float_bytes(housekeeper_MSI_string, &(heater.summary_buffer[13]), &str_index);  // heater.current
		sread_float_bytes(housekeeper_MSI_string, &(heater.summary_buffer[17]), &str_index);  // heater.voltage
		sread_float_bytes(housekeeper_MSI_string, &(heater.summary_buffer[21]), &str_index);  // heater.temperature
	}


	// Read gas pressure
	//
	struct pressureStruct pressure;
	if (err == 0)
	{
		for (int i = 0; i < 4; i++) pressure.summary_buffer[0 + i] = shot_number_bytes[i];
		for (int i = 0; i < 8; i++) pressure.summary_buffer[4 + i] = timestamp_bytes[i];

		sread_bool_byte(housekeeper_MSI_string, &(pressure.summary_buffer[12]), &str_index);  // pressure.ion_gauge_data_valid
		sread_bool_byte(housekeeper_MSI_string, &(pressure.summary_buffer[13]), &str_index);  // pressure.RGA_data_valid
		sread_float_bytes(housekeeper_MSI_string, &(pressure.summary_buffer[14]), &str_index);  // pressure.fill_pressure
		sread_float_bytes(housekeeper_MSI_string, &(pressure.summary_buffer[18]), &str_index);  // pressure.peak_AMU

		pressure.pressure_count = sread_int(housekeeper_MSI_string, &str_index);
		pressure.RGA_partial_pressure = (float*)malloc(pressure.pressure_count * sizeof(float));
		sread_float_array(housekeeper_MSI_string, pressure.RGA_partial_pressure, pressure.pressure_count, &str_index);
	}
	else
	{
		pressure.RGA_partial_pressure = (float*)malloc(1024 * sizeof(float));
	}


	// Read magnetic field
	//
	struct magneticFieldStruct magnetic_field;
	if (err == 0)
	{
		for (int i = 0; i < 4; i++) magnetic_field.summary_buffer[0 + i] = shot_number_bytes[i];
		for (int i = 0; i < 8; i++) magnetic_field.summary_buffer[4 + i] = timestamp_bytes[i];

		sread_bool_byte(housekeeper_MSI_string, &(magnetic_field.summary_buffer[12]), &str_index);  // magnetic_field.data_valid

		magnetic_field.profile_count = sread_int(housekeeper_MSI_string, &str_index);
		magnetic_field.profile = (float*)malloc(magnetic_field.profile_count * sizeof(float));
		sread_float_array(housekeeper_MSI_string, magnetic_field.profile, magnetic_field.profile_count, &str_index);

		magnetic_field.current_count = sread_int(housekeeper_MSI_string, &str_index);
		magnetic_field.supply_current = (float*)malloc(magnetic_field.current_count * sizeof(float));
		sread_float_array(housekeeper_MSI_string, magnetic_field.supply_current, magnetic_field.current_count, &str_index);

		sread_float_bytes(housekeeper_MSI_string, &(magnetic_field.summary_buffer[13]), &str_index);  // magnetic_field.peak_field
	}
	else
	{
		magnetic_field.profile = (float*)malloc(1024 * sizeof(float));
		magnetic_field.supply_current = (float*)malloc(1024 * sizeof(float));
	}


	// Read interferometers
	//
	struct interferometerStruct* interferometer;
	int interferometer_count;
	if (err == 0)
	{
		interferometer_count = sread_int(housekeeper_MSI_string, &str_index);
		interferometer = (interferometerStruct*)malloc(interferometer_count * sizeof interferometerStruct);
		for (int j = 0; j < interferometer_count; j++)
		{
			for (int i = 0; i < 4; i++) interferometer[j].summary_buffer[0 + i] = shot_number_bytes[i];
			for (int i = 0; i < 8; i++) interferometer[j].summary_buffer[4 + i] = timestamp_bytes[i];

			sread_bool_byte(housekeeper_MSI_string, &(interferometer[j].summary_buffer[12]), &str_index);  // interferometer[j].data_valid

			interferometer[j].density_count = sread_int(housekeeper_MSI_string, &str_index);
			interferometer[j].density = (float*)malloc(interferometer[j].density_count * sizeof(float));
			sread_float_array(housekeeper_MSI_string, interferometer[j].density, interferometer[j].density_count, &str_index);

			sread_float_bytes(housekeeper_MSI_string, &(interferometer[j].summary_buffer[13]), &str_index);  // interferometer[j].peak_density
		}
	}
	else
	{
		interferometer_count = 1;
		interferometer = (interferometerStruct*)malloc(interferometer_count * sizeof interferometerStruct);
		interferometer[0].density = (float*)malloc(1024 * sizeof(float));
	}


	// *************************************************
	// *** Process each MSI dataset in the HDF5 file ***
	//
	// This involves, for each of the many datasets:
	//   1) looking for the dataset
	//   2) if found, extending it
	//   3) if not found, create it
	//   4) write the shot into it
	//   5) closing the id's that were opened along the way
	//
	// First, though, declare the id's that we'll need
	//
	hid_t group_id;
	hid_t dataset_id;
	hid_t file_space_id;
	hid_t mem_space_id;
	hid_t mem_type_id;

	bool found;


	fprintf(stderr, "About to write to HDF5 file\n");  fflush(stdout);

	// :::::::::::::::::::::::::: /MSI/Discharge ::::::::::::::::::::::::::
	// ::::::: /MSI/Discharge/Discharge current
	//
	fprintf(stderr, "Writing /MSI/Discharge/Discharge current\n");  fflush(stdout);
	if (err == 0)
		fprintf(stderr, "About to find_dataset()\n");  fflush(stdout);
	err = find_dataset(file_id, "/MSI/Discharge", "Discharge current", &group_id, &found);
	fprintf(stderr, "find_dataset() -> found: %d\n", found);  fflush(stdout);
	if (err == 0)
		fprintf(stderr, "About to write to extend_create_2D_MSI_dataset() with index: %d\n", index);  fflush(stdout);
	err = extend_create_2D_MSI_dataset("Discharge current", found, group_id,
		index, discharge.current_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		fprintf(stderr, "About to H5Dwrite()\n");  fflush(stdout);
	err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.current);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	fprintf(stderr, "About to close up for /MSI/Discharge/Discharge current\n");  fflush(stdout);
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);


	// ::::::: /MSI/Discharge/Cathode-anode voltage
	//
	fprintf(stderr, "Writing /MSI/Discharge/Cathode-anode voltage\n");  fflush(stdout);
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Discharge", "Cathode-anode voltage", &group_id, &found);
	if (err == 0)
		err = extend_create_2D_MSI_dataset("Cathode-anode voltage", found, group_id,
			index, discharge.voltage_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.cathode_anode_voltage);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);


	// ::::::: /MSI/Discharge/Discharge summary
	//
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Discharge", "Discharge summary", &group_id, &found);
	if (err == 0)
		err = extend_create_summary_dataset("Discharge summary",
			found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.summary_buffer);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);



	// :::::::::::::::::::::::::: /MSI/Heater ::::::::::::::::::::::::::
	// ::::::: /MSI/Heater/Heater summary
	//
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Heater", "Heater summary", &group_id, &found);
	if (err == 0)
		err = extend_create_summary_dataset("Heater summary", found, group_id,
			index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, heater.summary_buffer);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);



	// :::::::::::::::::::::::::: /MSI/Gas pressure ::::::::::::::::::::::::::
	// ::::::: /MSI/Gas pressure/Discharge current
	//
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Gas pressure", "RGA partial pressures", &group_id, &found);
	if (err == 0)
		err = extend_create_2D_MSI_dataset("RGA partial pressures", found, group_id,
			index, pressure.pressure_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, pressure.RGA_partial_pressure);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);


	// ::::::: /MSI/Gas pressure/Gas pressure summary
	//
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Gas pressure", "Gas pressure summary", &group_id, &found);
	if (err == 0)
		err = extend_create_summary_dataset("Gas pressure summary", found, group_id,
			index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, pressure.summary_buffer);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);



	// :::::::::::::::::::::::::: /MSI/Magnetic field ::::::::::::::::::::::::::
	// ::::::: /MSI/Magnetic field/Magnetic field profile
	//
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Magnetic field", "Magnetic field profile", &group_id, &found);
	if (err == 0)
		err = extend_create_2D_MSI_dataset("Magnetic field profile", found, group_id,
			index, magnetic_field.profile_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.profile);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);


	// ::::::: /MSI/Magnetic field/Magnet power supply currents
	//
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Magnetic field", "Magnet power supply currents", &group_id, &found);
	if (err == 0)
		err = extend_create_2D_MSI_dataset("Magnet power supply currents", found, group_id,
			index, magnetic_field.current_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.supply_current);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);


	// ::::::: /MSI/Magnetic field/Magnetic field summary
	//
	if (err == 0)
		err = find_dataset(file_id, "/MSI/Magnetic field", "Magnetic field summary", &group_id, &found);
	if (err == 0)
		err = extend_create_summary_dataset("Magnetic field summary", found, group_id,
			index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
	if (err == 0)
		err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.summary_buffer);
	if (err < 0)
	{
		write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
	}
	H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);


	// Interferometer array
	//
	char group_name[1024];
	for (int j = 0; j < interferometer_count; j++)
	{
		sprintf_s(group_name, 1024, "/MSI/Interferometer array/Interferometer [%d]", j);


		// ::::::: /MSI/Interferometer array/Interferometer[j]/Interferometer trace
		//
		if (err == 0)
			err = find_dataset(file_id, group_name, "Interferometer trace", &group_id, &found);
		if (err == 0)
			err = extend_create_2D_MSI_dataset("Interferometer trace", found, group_id,
				index, interferometer[j].density_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
		if (err == 0)
			err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, interferometer[j].density);
		if (err < 0)
		{
			write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
		}
		H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);


		// ::::::: /MSI/Interferometer array/Interferometer[j]/Interferometer summary list
		//
		fprintf(stderr, "Writing /MSI/Interferometer array/Interferometer[j]/Interferometer summary list\n");  fflush(stdout);
		if (err == 0)
			err = find_dataset(file_id, group_name, "Interferometer summary list", &group_id, &found);
		if (err == 0)
			err = extend_create_summary_dataset("Interferometer summary list", found, group_id,
				index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id);
		if (err == 0)
			err = H5Dwrite(dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, interferometer[j].summary_buffer);
		if (err < 0)
		{
			write_hdf5_error(efn, shot_number); insert_hdf5_error(file_id, error_index, shot_number);
		}
		H5Tclose(mem_type_id); H5Sclose(mem_space_id); H5Sclose(file_space_id); H5Dclose(dataset_id); H5Gclose(group_id);
	}


	// **********************************************
	// *** Free up HDF5 id's and allocated memory ***
	//
	fprintf(stderr, "Freeing up everything\n");  fflush(stdout);
	free(discharge.current);
	free(discharge.cathode_anode_voltage);
	free(pressure.RGA_partial_pressure);
	free(magnetic_field.profile);
	free(magnetic_field.supply_current);
	for (int j = 0; j < interferometer_count; j++)
		free(interferometer[j].density);
	free(interferometer);

	free(timestamp);
	free(shot_number_ptr);

	H5Gclose(group_id);

	*MSI_index = index + 1;

	return err;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Insert MSI
//
herr_t insert_MSI_generic(
	_TCHAR* argv[],
	char* data_run_id,
	hid_t file_id,
	int shot_number,
	int* MSI_index,
	int* error_index)
{
	// Read in the arguments
	//
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];
	char root_folder[1024];

	strcpy_s(experiment_set, 1024, argv[2]);
	strcpy_s(experiment, 1024, argv[3]);
	strcpy_s(data_run, 1024, argv[4]);
	strcpy_s(root_folder, 1024, argv[5]);

	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf_s(experiment_folder, 1024, "%s/%s/%s", root_folder, experiment_set, experiment);
	sprintf_s(data_run_folder, 1024, "%s/%s", experiment_folder, data_run);

	char efn[1024];  // efn == error_filename, shortened to allow single line error-handling
	sprintf_s(efn, 1024, "%s/Error.C++.dat", data_run_folder);

	int str_index = 0;
	char MSI_data_run_id_name[1024];
	for (int i = 0; i < 1024 - str_index; ++i)
	{
		if (data_run_id[str_index] == ' ')
		{
			MSI_data_run_id_name[i] = '\0';
			str_index++;
			break;
		}
		MSI_data_run_id_name[i] = data_run_id[str_index];
		str_index++;
	}


	char MSI_data_run_id_value[1024];
	for (int i = 0; i < 1024 - str_index; ++i)
	{
		if (data_run_id[str_index] == ' ')
		{
			MSI_data_run_id_value[i] = '\0';
			str_index++;
			break;
		}
		MSI_data_run_id_value[i] = data_run_id[str_index];
		str_index++;
	}

	char MSI_experiment_set[1024];
	for (int i = 0; i < 1024 - str_index; ++i)
	{
		MSI_experiment_set[i] = data_run_id[str_index];
		if (data_run_id[str_index] == '\0') break;
		str_index++;
	}

	char MSI_data_run_folder[1024];
	sprintf_s(MSI_data_run_folder, 1024, "%s/%s/%s-%s-MSI", root_folder, MSI_experiment_set, MSI_data_run_id_name, MSI_data_run_id_value);


	// **************************************************************
	// *** Open and read the MSI string and data descriptor files ***
	//
	// Open and read the MSI data descriptor file
	// Starting in June, 2022, the structure of the MSI string file is decribed in the MSI_descriptor.  Before this, the structure was
	// hard-coded in this function, which made it very hard to generalize.  The generalization can take 2 forms:
	//   - the original MSI (from the Housekeeper) can be modified
	//   - other MSI information from other sources can be added
	//
	// The question is how to manage the transition.  This is discussed in detail here: <TODO: add link to documentation>.  Briefly:
	//   - it has to be backward-compatible in the sense that it still works for older data runs
	//   - the new HDF5 file format will match the old format as much as possible but small changes are acceptable
	//     - this might necessitate small changes to software that consumes the HDF5 files
	//
	char MSI_descriptor_filename[2048];
	sprintf_s(MSI_descriptor_filename, 2048, "%s/MSI_descriptor.%06d.json", MSI_data_run_folder, shot_number);
	std::cout << "MSI_descriptor_filename: " << MSI_descriptor_filename << std::endl;
	std::ifstream descriptor_file(MSI_descriptor_filename);
	if (descriptor_file.fail())
	{
		std::cout << "About to call insert_MSI_legacy()" << std::endl;
		return insert_MSI_legacy(file_id, data_run_folder, efn, shot_number, MSI_index, error_index);
	}
	else
	{
		std::cout << "About to call insert_MSI_generic()" << std::endl;
		return insert_MSI_generic(descriptor_file, file_id, MSI_data_run_folder, efn, shot_number, MSI_index, error_index);
	}
}


