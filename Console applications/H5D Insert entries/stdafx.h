// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#include <iostream>
#include <tchar.h>
#include "stdio.h"
#include <string>
#include <signal.h>
#include <windows.h>
#include "hdf5.h"

// TODO: reference additional headers your program requires here
#include "Read routines.h"
#include "Data structures.h"
#include "Error handling.h"
#include "Insert devices.h"
#include "Insert MSI.h"
#include "Insert MSI - generic.h"
#include "Insert sequence lines.h"
#include "Insert configs.h"
#include "Average SIS crate.h"

#define JSON_DLL
