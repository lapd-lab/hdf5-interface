//========================================================================================
// H5D Insert entries.cpp : Defines the entry point for the console application.
//
// Usage:
//   H5D_Insert_entries.exe <HDF5 file> <experiment set> <experiment> <data run> <device count>
//     {<device name> list}
//
// <HDF5 file>: full path of the HDF5 file in which to insert a single shot of device data
// <experiment set> <experiment> <data run>: 3 strings which locate the shot files
// <device count>: the number of devices in the data run
// {<device name> list}: the device names surrounded by double quotes, i.e. "SIS 3301" "6K Compumotor"
//
// Writes an error file if an error occurs.
// Writes an exit file when finished.
//
// This is based on H5D_Insert_devices.cpp.  It has been changed to loop until reading a
// termination flag.  It works on a single HDF5 file, opening and closing it on each shot.
// It also inserts all types of entries, namely:
//   -errors
//   -MSI
//   -sequence lines
//   -devices
//   -device configs
//


#include "stdafx.h"


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	// Expecting "H5D_Insert_devices.exe" plus 5+ndevices+1 arguments
	//
	// Read in the first 5 arguments
	//
	char HDF5_filename[1024];
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];
	int device_count;

	strcpy( HDF5_filename, argv[1] );
	strcpy( experiment_set, argv[2] );
	strcpy( experiment, argv[3] );
	strcpy( data_run, argv[4] );

	char root_folder[1024] = "C:/Shadow data";
	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf( experiment_folder, "%s/%s/%s", root_folder, experiment_set, experiment );
	sprintf( data_run_folder, "%s/%s", experiment_folder, data_run );

	char efn[1024];  // efn == error_out_filename, shortened to allow single line error-handling
	char exit_filename[1024];
	char log_filename[1024];
	sprintf( efn, "%s/Error.C++.dat", data_run_folder );
	sprintf( exit_filename, "%s/Exit.C++.dat", data_run_folder );
	sprintf( log_filename, "%s/Log.C++.dat", data_run_folder );

	device_count = atoi(argv[5]);
	if ( argc != 7+device_count )
	{
		write_error( efn, 0, "Incorrect argument count.  Error exit." );
		FILE* exit_file = fopen( exit_filename, "w" );
		fprintf( exit_file, "Incorrect argument count.  Error exit.\n" );
		fclose( exit_file );
		return -1;
	}


	// Print out the arguments
	//
	fprintf( stdout, "H5D_Insert_entries.exe beginning...\n\n" );
	fprintf( stdout, "HDF5 filename: %s\n", HDF5_filename );
	fprintf( stdout, "Experiment set: %s\n", experiment_set );
	fprintf( stdout, "Experiment: %s\n", experiment );
	fprintf( stdout, "Data run: %s\n", data_run );
	fprintf( stdout, "Device count: %d\n", device_count );

	FILE* log_file = fopen( log_filename, "w" );
	fprintf( log_file, "H5D_Insert_entries.exe beginning...\n\n" );
	fprintf( log_file, "HDF5 filename: %s\n", HDF5_filename );
	fprintf( log_file, "Experiment set: %s\n", experiment_set );
	fprintf( log_file, "Experiment: %s\n", experiment );
	fprintf( log_file, "Data run: %s\n", data_run );
	fprintf( log_file, "Device count: %d\n", device_count );

	for ( int idevice = 0; idevice<device_count; idevice++ )
	{
		char device_name[1024];
		strcpy( device_name, argv[6+idevice] );
		fprintf( stdout, "  %s\n", device_name );
		fprintf( log_file, "  %s\n", device_name );
	}

	int timestamp;
	timestamp = atoi(argv[6+device_count]);
	fprintf( stdout, "timestamp: %d\n", timestamp );
	fprintf( log_file, "timestamp: %d\n", timestamp );
	fflush( stdout );


	// Setup for the loop
	//
	H5Eset_auto( NULL, NULL );  // turns off automatic printing of HDF5 errors
	hid_t file_id;

	char devices_filename[1024];
	char error_filename[1024];
	char MSI_filename[1024];
	char sequence_lines_filename[1024];
	char shot_signal_filename[1024];

	herr_t err = 0;
	int shot_number = 0;
	int sleep_count = 0;
	int max_sleep_count = 100000;
	int translation_complete = 0;

	sprintf( devices_filename, "%s/Devices/Devices.%06d.dat", data_run_folder, shot_number );
	sprintf( error_filename, "%s/Error/Error.%06d.dat", data_run_folder, shot_number );
	sprintf( MSI_filename, "%s/MSI/MSI.%06d.dat", data_run_folder, shot_number );
	sprintf( sequence_lines_filename, "%s/Sequence lines/Sequence lines.%06d.dat", data_run_folder, shot_number );
	sprintf( shot_signal_filename, "%s/Shot/Shot.%06d.dat", data_run_folder, shot_number );

	int devices_index = 0;
	int error_index = 0;
	int MSI_index = 0;
	int sequence_lines_index = 0;
	int file_timestamp = 0;


	// Loop indefinitely, processing "Shot.######.dat" files.  Keep track of time
	// since the last file appeared and stop when the time is too long, say
	// 100,000 seconds, i.e. a little more than 1 day.  Also, look within each
	// file for the "translation complete" flag to be true
	//
	while ( (sleep_count < max_sleep_count) && (translation_complete == 0) )
	{
		// Start by sleeping for one second
		//
		Sleep( 1000 );
		sleep_count++;

		err = 0;
		file_timestamp = 0;


		// Look for the shot signal file
		//
		FILE* shot_signal_file = fopen( shot_signal_filename, "rb" );
		if ( shot_signal_file != NULL ) file_timestamp = read_int( shot_signal_file );
		if ( file_timestamp == timestamp )
		{
			fprintf( stdout, "Shot %d: ", shot_number );
			fprintf( log_file, "Shot %d: ", shot_number );
			fflush( stdout );
			fflush( log_file );


			// Read translation complete flag
			translation_complete = read_int( shot_signal_file );
			fclose( shot_signal_file );


			// Open the HDF5 file
			//
			file_id = H5Fopen( HDF5_filename, H5F_ACC_RDWR, H5P_DEFAULT );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
			if ( file_id < 0 )
			{
				write_hdf5_error( efn, 0 );
				FILE* exit_file = fopen( exit_filename, "w" );
				fprintf( exit_file, "Could not open HDF5 file.  Error exit.\n" );
				fprintf( log_file, "Could not open HDF5 file.  Error exit.\n" );
				fclose( exit_file );
				fclose( log_file );
				return -1;
			}


			// Do all the entries
			//
			// Devices
			if ( err == 0 )
			{
				FILE* devices_file = fopen( devices_filename, "rb" );
				if ( devices_file != NULL )
				{
					fclose( devices_file );
					fprintf( stdout, "Devices " );
					fprintf( log_file, "Devices " );
					fflush( stdout );
					fflush( log_file );
					err = insert_devices( argv, file_id, shot_number, &devices_index, &error_index );
					if ( err == 0 )
						err = insert_configs( argv, file_id, shot_number, &error_index);
				}
			}

			// MSI
			if ( err == 0 )
			{
				FILE* MSI_file = fopen( MSI_filename, "rb" );
				if ( MSI_file != NULL )
				{
					fclose( MSI_file );
					fprintf( stdout, "MSI " );
					fprintf( log_file, "MSI " );
					fflush( stdout );
					fflush( log_file );
					err = insert_MSI( argv, file_id, shot_number, &MSI_index, &error_index );
				}
			}

			// Sequence lines
			if ( err == 0 )
			{
				FILE* sequence_lines_file = fopen( sequence_lines_filename, "rb" );
				if ( sequence_lines_file != NULL )
				{
					fclose( sequence_lines_file );
					fprintf( stdout, "Sequence lines " );
					fprintf( log_file, "Sequence lines " );
					fflush( stdout );
					fflush( log_file );
					err = insert_sequence_lines( argv, file_id, shot_number, &sequence_lines_index, &error_index );
				}
			}

			// Error
			if ( err == 0 )
			{
				FILE* error_file = fopen( error_filename, "rb" );
				if ( error_file != NULL )
				{
					fprintf( stdout, "Error " );
					fprintf( log_file, "Error " );
					fflush( stdout );
					fflush( log_file );

					char error_string[4096];  // dictate that error strings cannot be longer than 4096 characters
					read_string( error_file, error_string, 4096 );
					fclose( error_file );
					insert_error( file_id, &error_index, shot_number, error_string );
				}
			}


			H5Fclose( file_id );

			sleep_count = 0;
			shot_number++;
			sprintf( devices_filename, "%s/Devices/Devices.%06d.dat", data_run_folder, shot_number );
			sprintf( error_filename, "%s/Error/Error.%06d.dat", data_run_folder, shot_number );
			sprintf( MSI_filename, "%s/MSI/MSI.%06d.dat", data_run_folder, shot_number );
			sprintf( sequence_lines_filename, "%s/Sequence lines/Sequence lines.%06d.dat", data_run_folder, shot_number );
			sprintf( shot_signal_filename, "%s/Shot/Shot.%06d.dat", data_run_folder, shot_number );

			fprintf( stdout, "\n" );
			fprintf( log_file, "\n" );
			fflush( stdout );
		} // End if shot signal file was found and timestamps match
	} // End while loop

	FILE* exit_file = fopen( exit_filename, "w" );
	if ( err == 0 )
	{
		fprintf( exit_file, "Normal exit.\n" );
		fprintf( stdout, "Normal exit.\n" );
		fprintf( log_file, "Normal exit.\n" );
	}
	else
	{
		fprintf( exit_file, "Error exit.\n" );
		fprintf( stdout, "Error exit.\n" );
		fprintf( log_file, "Error exit.\n" );
	}
	fflush( stdout );
	fclose( exit_file );

	fprintf( log_file, "translation_complete: %d\n", translation_complete );
	fprintf( log_file, "sleep_count: %d\n", sleep_count );
	fprintf( log_file, "max_sleep_count: %d\n", max_sleep_count );
	fprintf( log_file, "timestamp: %d\n", timestamp );
	fprintf( log_file, "file_timestamp: %d\n", file_timestamp );
	fclose( log_file );

	return err;
}


