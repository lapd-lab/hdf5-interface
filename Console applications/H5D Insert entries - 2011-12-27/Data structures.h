//========================================================================================
// Data structures.h : Various data structures to match the LabVIEW clusters.
//
//


struct dischargeStruct
{
	int current_count;
	float *current;
	int voltage_count;
	float *cathode_anode_voltage;
	char summary_buffer[25];
};

struct heaterStruct
{
	char summary_buffer[25];
};

struct pressureStruct
{
	int pressure_count;
	float *RGA_partial_pressure;
	char summary_buffer[22];
};

struct magneticFieldStruct
{
	int profile_count;
	float *profile;
	int current_count;
	float *supply_current;
	char summary_buffer[17];
};

struct interferometerStruct
{
	int density_count;
	float *density;
	char summary_buffer[17];
};

struct dataElement
{
	char LV_type;
	char* name;
	short dims;
	short enum_count;
	char** enum_names;
	short sub_element_count;
	short* sub_elements;
	dataElement* sub_reference_list;
	short sub_descriptor_count;
};

