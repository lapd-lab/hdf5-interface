//========================================================================================
// Test.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <vector>

class Foo
{
public:
	explicit Foo(char* str): m_str(str)	{}

	~Foo()
	{
		if (m_str != NULL) delete[] m_str;
	}
	
private:
	char* m_str;
};


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
    std::vector<Foo> v;
	v.push_back(Foo(new char[20]));

    return 0;
}


