//========================================================================================
// H5D Insert MSI.cpp : Defines the entry point for the console application.
//
// Usage:
//   H5D_Insert_MSI.exe <HDF5 file> <index> <shot number> <MSI string length>
//
// <HDF5 file>: full path of the HDF5 file in which to insert a single shot of MSI data
// <index>: location within each dataset to insert the data
// <shot number>: usually not the same as index
// <MSI string length>: number of bytes of flattened data written to "C:\Temp\MSI string.txt"
//
// No output file.
//
//
// In this file:
//
//	herr_t find_dataset(
//		hid_t file_id,
//		char *group_name,
//		char *dataset_name,
//		hid_t *group_id,
//		bool *found)
//
//	herr_t extend_create_2D_dataset(
//		char* dataset,
//		bool found,
//		hid_t group_id,
//		int index,
//		int data_length,
//		hid_t* dataset_id,
//		hid_t* file_space_id,
//		hid_t* mem_space_id,
//		hid_t* mem_type_id )
//
//	herr_t extend_create_summary_dataset(
//		char* dataset,
//		bool found,
//		hid_t group_id,
//		int index,
//		hid_t* dataset_id,
//		hid_t* file_space_id,
//		hid_t* mem_space_id,
//		hid_t* mem_type_id )
//	
//   int _tmain(int argc, _TCHAR* argv[])


#include "stdafx.h"
#include "hdf5.h"


//----------------------------------------------------------------------------------------
// Find dataset
//
herr_t find_dataset(
	hid_t file_id,
	char *group_name,
	char *dataset_name,
	hid_t *group_id,
	bool *found)
{
	*found = false;
	fprintf( stdout, "Looking for group: %s...\n", group_name );
	*group_id = H5Gopen( file_id, group_name );
	if ( group_id < 0 ) { H5Eprint( stderr ); return -1; }

	hsize_t num_obj;
	herr_t err = H5Gget_num_objs( *group_id, &num_obj );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Loop through objects, looking for a match with the dataset name
	//
	fprintf( stdout, "Looking for dataset: %s...\n", dataset_name );
	char obj_name[1024];
	for ( hsize_t idx=0; idx<num_obj; idx++ )
	{
		ssize_t size = H5Gget_objname_by_idx( *group_id, idx, obj_name, 1024 );
		if ( size < 0 ) { H5Eprint( stderr ); return -1; }
		if ( strcmp(obj_name, dataset_name) == 0 ) { *found = true; break; }
	}
	if ( *found ) fprintf( stdout, "%s found\n", dataset_name );
	else fprintf( stdout, "%s not found\n", dataset_name );

	return 0;
}

	
//----------------------------------------------------------------------------------------
// Extend/Create 2D dataset
//
herr_t extend_create_2D_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	int data_length,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	if ( found ) fprintf( stdout, "\nExtending dataset: " );
	else fprintf( stdout, "\nCreating dataset: " );
	fprintf( stdout, "%s...\n", dataset );
	herr_t err;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen( group_id, dataset );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }
	}


	// File dataspace
	//
	int file_rank = 2;
	hsize_t file_dims[2] = { index+1, data_length };
	hsize_t file_maxdims[2] = { 4294967295, data_length };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
	}

	hsize_t start[2] = { index, 0 };
	hsize_t stride[2] = { 1, 1 };
	hsize_t count[2] = { 1, 1 };
	hsize_t block[2] = { 1, data_length };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { data_length };
	hsize_t mem_maxdims[1] = { data_length };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	*mem_type_id = H5Tcopy( H5T_NATIVE_FLOAT );
	if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t file_chunk[2] = { 1, data_length };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate( group_id, dataset, *mem_type_id, *file_space_id, plist_id );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

		H5Pclose( plist_id );
	}

	return 0;
}


//----------------------------------------------------------------------------------------
// Extend/Create summary dataset
//
herr_t extend_create_summary_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	if ( found ) fprintf( stdout, "\nExtending dataset: " );
	else fprintf( stdout, "\nCreating dataset: " );
	fprintf( stdout, "%s...\n", dataset );
	herr_t err;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen( group_id, dataset );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }
	}


	// File dataspace
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { index+1 };
	hsize_t file_maxdims[1] = { 4294967295 };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
	}

	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { 1 };
	hsize_t block[1] = { 1 };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { 1 };
	hsize_t mem_maxdims[1] = { 1 };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory datatype
	// This is the hard part because there are a number of different datatypes, should have a
	// function call for it.
	//
	hid_t char_field_id = H5Tcopy( H5T_NATIVE_CHAR );
	hid_t int_field_id = H5Tcopy( H5T_NATIVE_INT );
	hid_t float_field_id = H5Tcopy( H5T_NATIVE_FLOAT );
	hid_t double_field_id = H5Tcopy( H5T_NATIVE_DOUBLE );
	if ( strcmp(dataset, "Discharge summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 25 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Pulse length", 13, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Peak current", 17, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Bank voltage", 21, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	if ( strcmp(dataset, "Heater summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 25 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Heater current", 13, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Heater voltage", 17, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Heater temperature", 21, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	if ( strcmp(dataset, "Gas pressure summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 22 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Ion gauge data valid", 12, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "RGA data valid", 13, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Fill pressure", 14, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Peak AMU", 18, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	if ( strcmp(dataset, "Magnetic field summary") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 17 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Peak magnetic field", 13, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	if ( strcmp(dataset, "Interferometer summary list") == 0 )
	{
		*mem_type_id = H5Tcreate( H5T_COMPOUND, 17 );
		if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Shot number", 0, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Timestamp", 4, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Data valid", 12, char_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Tinsert( *mem_type_id, "Peak density", 13, float_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}

	H5Tclose( char_field_id );  H5Tclose( int_field_id );  H5Tclose( float_field_id );  H5Tclose( double_field_id );


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate( group_id, dataset, *mem_type_id, *file_space_id, plist_id );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

		H5Pclose( plist_id );
	}

	return 0;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	// Expecting "H5D_Insert_MSI.exe" plus 4 arguments
	//
	if ( argc != 5 )
	{
		fprintf( stderr, "argc = %d\n", argc );
		return -1;
	}


	// Read in the arguments
	//
	char HDF5_filename[1024];
	int index;
	int *shot_number = (int *)malloc( sizeof(int) );
	char *shot_number_bytes;
	int MSI_string_length;

	strcpy( HDF5_filename, argv[1] );
	index = atoi(argv[2]);
	*shot_number = atoi(argv[3]);
	shot_number_bytes = (char *) shot_number;
	MSI_string_length = atoi(argv[4]);


	// Open the HDF5 file
	//
	hid_t file_id = H5Fopen( HDF5_filename, H5F_ACC_RDWR, H5P_DEFAULT );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
	if ( file_id < 0 ) { H5Eprint( stderr ); return -1; }


	// *****************************************
	// *** Open and read the MSI string file ***
	//
	// Open the MSI string file
	//
	FILE* MSI_string_file = fopen( "C:/Temp/MSI string.txt", "rb" );
	int reported_length = read_int( MSI_string_file );
	if ( reported_length != MSI_string_length )
	{
		fprintf( stderr, "Expected length (bytes): %d\n", MSI_string_length );
		fprintf( stderr, "Reported length (bytes): %d\n", reported_length );
		fflush( stderr );
		return -1;
	}
	char *MSI_string = (char *)malloc( MSI_string_length );
	// size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
	size_t nread = fread( MSI_string, 1, MSI_string_length, MSI_string_file );
	fclose( MSI_string_file );

	fprintf( stdout, "nread: %d\n", nread );
	fflush( stdout );
	if ( nread != MSI_string_length )
	{
		int is_eof = feof( MSI_string_file );
		fprintf( stderr, "is_eof: %d\n", is_eof );
		fflush( stderr );
		return -1;
	}


	// Read timestamp
	//
	int str_index = 0;
	double timestamp_value = sread_double( MSI_string, &str_index );
	double *timestamp = (double *)malloc( sizeof(double) );
	char *timestep_bytes;
	*timestamp = timestamp_value;
	timestep_bytes = (char *) timestamp;


	// Read discharge
	//
	struct dischargeStruct discharge;
	for ( int i=0; i<4; i++ ) discharge.summary_buffer[ 0+i] = shot_number_bytes[i];
	for ( int i=0; i<8; i++ ) discharge.summary_buffer[ 4+i] = timestep_bytes[i];

	sread_bool_byte( MSI_string, &(discharge.summary_buffer[12]), &str_index );  // discharge.data_valid

	discharge.current_count = sread_int( MSI_string, &str_index );
	discharge.current = (float *)malloc( discharge.current_count * sizeof(float) );
	sread_float_array( MSI_string, discharge.current, discharge.current_count, &str_index );

	discharge.voltage_count = sread_int( MSI_string, &str_index );
	discharge.cathode_anode_voltage = (float *)malloc( discharge.voltage_count * sizeof(float) );
	sread_float_array( MSI_string, discharge.cathode_anode_voltage, discharge.voltage_count, &str_index );

	sread_float_bytes( MSI_string, &(discharge.summary_buffer[13]), &str_index );  // discharge.pulse_length 
	sread_float_bytes( MSI_string, &(discharge.summary_buffer[17]), &str_index );  // discharge.peak_current
	sread_float_bytes( MSI_string, &(discharge.summary_buffer[21]), &str_index );  // discharge.bank_voltage


	// Read heater
	//
	struct heaterStruct heater;
	for ( int i=0; i<4; i++ ) heater.summary_buffer[ 0+i] = shot_number_bytes[i];
	for ( int i=0; i<8; i++ ) heater.summary_buffer[ 4+i] = timestep_bytes[i];

	sread_bool_byte( MSI_string, &(heater.summary_buffer[12]), &str_index );  // heater.data_valid
	sread_float_bytes( MSI_string, &(heater.summary_buffer[13]), &str_index );  // heater.current
	sread_float_bytes( MSI_string, &(heater.summary_buffer[17]), &str_index );  // heater.voltage
	sread_float_bytes( MSI_string, &(heater.summary_buffer[21]), &str_index );  // heater.temperature


	// Read gas pressure
	//
	struct pressureStruct pressure;
	for ( int i=0; i<4; i++ ) pressure.summary_buffer[ 0+i] = shot_number_bytes[i];
	for ( int i=0; i<8; i++ ) pressure.summary_buffer[ 4+i] = timestep_bytes[i];

	sread_bool_byte( MSI_string, &(pressure.summary_buffer[12]), &str_index );  // pressure.ion_gauge_data_valid
	sread_bool_byte( MSI_string, &(pressure.summary_buffer[13]), &str_index );  // pressure.RGA_data_valid
	sread_float_bytes( MSI_string, &(pressure.summary_buffer[14]), &str_index );  // pressure.fill_pressure
	sread_float_bytes( MSI_string, &(pressure.summary_buffer[18]), &str_index );  // pressure.peak_AMU

	pressure.pressure_count = sread_int( MSI_string, &str_index );
	pressure.RGA_partial_pressure = (float *)malloc( pressure.pressure_count * sizeof(float) );
	sread_float_array( MSI_string, pressure.RGA_partial_pressure, pressure.pressure_count, &str_index );


	// Read magnetic field
	//
	struct magneticFieldStruct magnetic_field;
	for ( int i=0; i<4; i++ ) magnetic_field.summary_buffer[ 0+i] = shot_number_bytes[i];
	for ( int i=0; i<8; i++ ) magnetic_field.summary_buffer[ 4+i] = timestep_bytes[i];

	sread_bool_byte( MSI_string, &(magnetic_field.summary_buffer[12]), &str_index );  // magnetic_field.data_valid

	magnetic_field.profile_count = sread_int( MSI_string, &str_index );
	magnetic_field.profile = (float *)malloc( magnetic_field.profile_count * sizeof(float) );
	sread_float_array( MSI_string, magnetic_field.profile, magnetic_field.profile_count, &str_index );

	magnetic_field.current_count = sread_int( MSI_string, &str_index );
	magnetic_field.supply_current = (float *)malloc( magnetic_field.current_count * sizeof(float) );
	sread_float_array( MSI_string, magnetic_field.supply_current, magnetic_field.current_count, &str_index );

	sread_float_bytes( MSI_string, &(magnetic_field.summary_buffer[13]), &str_index );  // magnetic_field.peak_field


	// Read interferometers
	//
	struct interferometerStruct *interferometer;
	int interferometer_count = sread_int( MSI_string, &str_index );
	interferometer = (interferometerStruct *)malloc( interferometer_count * sizeof interferometerStruct );
	for ( int j=0; j<interferometer_count; j++ )
	{
		for ( int i=0; i<4; i++ ) interferometer[j].summary_buffer[ 0+i] = shot_number_bytes[i];
		for ( int i=0; i<8; i++ ) interferometer[j].summary_buffer[ 4+i] = timestep_bytes[i];

		sread_bool_byte( MSI_string, &(interferometer[j].summary_buffer[12]), &str_index );  // interferometer[j].data_valid

		interferometer[j].density_count = sread_int( MSI_string, &str_index );
		interferometer[j].density = (float *)malloc( interferometer[j].density_count * sizeof(float) );
		sread_float_array( MSI_string, interferometer[j].density, interferometer[j].density_count, &str_index );

		sread_float_bytes( MSI_string, &(interferometer[j].summary_buffer[13]), &str_index );  // interferometer[j].peak_density
	}


	// *************************************************
	// *** Process each MSI dataset in the HDF5 file ***
	//
	// This involves, for each of the many datasets:
	//   1) looking for the dataset
	//   2) if found, extending it
	//   3) if not found, create it
	//   4) write the shot into it
	//   5) closing the id's that were opened along the way
	//
	// First, though, declare the id's that we'll need
	//
	hid_t group_id;
	hid_t dataset_id;
	hid_t file_space_id;
	hid_t mem_space_id;
	hid_t mem_type_id;

	herr_t err;
	bool found;



	// :::::::::::::::::::::::::: /MSI/Discharge ::::::::::::::::::::::::::
	// ::::::: /MSI/Discharge/Discharge current
	//
	err = find_dataset( file_id, "/MSI/Discharge", "Discharge current", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_2D_dataset( "Discharge current",
		found, group_id, index, discharge.current_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.current );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Discharge/Cathode-anode voltage
	//
	err = find_dataset( file_id, "/MSI/Discharge", "Cathode-anode voltage", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_2D_dataset( "Cathode-anode voltage",
		found, group_id, index, discharge.voltage_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.cathode_anode_voltage );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Discharge/Discharge summary
	//
	err = find_dataset( file_id, "/MSI/Discharge", "Discharge summary", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_summary_dataset( "Discharge summary",
		found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, discharge.summary_buffer );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );



	// :::::::::::::::::::::::::: /MSI/Heater ::::::::::::::::::::::::::
	// ::::::: /MSI/Heater/Heater summary
	//
	err = find_dataset( file_id, "/MSI/Heater", "Heater summary", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_summary_dataset( "Heater summary",
		found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, heater.summary_buffer );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );



	// :::::::::::::::::::::::::: /MSI/Gas pressure ::::::::::::::::::::::::::
	// ::::::: /MSI/Gas pressure/Discharge current
	//
	err = find_dataset( file_id, "/MSI/Gas pressure", "RGA partial pressures", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_2D_dataset( "RGA partial pressures",
		found, group_id, index, pressure.pressure_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, pressure.RGA_partial_pressure );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Gas pressure/Gas pressure summary
	//
	err = find_dataset( file_id, "/MSI/Gas pressure", "Gas pressure summary", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_summary_dataset( "Gas pressure summary",
		found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, pressure.summary_buffer );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );



	// :::::::::::::::::::::::::: /MSI/Magnetic field ::::::::::::::::::::::::::
	// ::::::: /MSI/Magnetic field/Magnetic field profile
	//
	err = find_dataset( file_id, "/MSI/Magnetic field", "Magnetic field profile", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_2D_dataset( "Magnetic field profile",
		found, group_id, index, magnetic_field.profile_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.profile );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Magnetic field/Magnet power supply currents
	//
	err = find_dataset( file_id, "/MSI/Magnetic field", "Magnet power supply currents", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_2D_dataset( "Magnet power supply currents",
		found, group_id, index, magnetic_field.current_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.supply_current );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// ::::::: /MSI/Magnetic field/Magnetic field summary
	//
	err = find_dataset( file_id, "/MSI/Magnetic field", "Magnetic field summary", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_summary_dataset( "Magnetic field summary",
		found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, magnetic_field.summary_buffer );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// Interferometer array
	//
	char group_name[1024];
	for ( int j=0; j<interferometer_count; j++ )
	{
		sprintf( group_name, "/MSI/Interferometer array/Interferometer [%d]", j );


		// ::::::: /MSI/Interferometer array/Interferometer[j]/Interferometer trace
		//
		err = find_dataset( file_id, group_name, "Interferometer trace", &group_id, &found );  if ( err < 0 ) return -1;
		err = extend_create_2D_dataset( "Interferometer trace",
			found, group_id, index, interferometer[j].density_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
		if ( err < 0 ) return -1;

		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, interferometer[j].density );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


		// ::::::: /MSI/Interferometer array/Interferometer[j]/Interferometer summary list
		//
		err = find_dataset( file_id, group_name, "Interferometer summary list", &group_id, &found );  if ( err < 0 ) return -1;
		err = extend_create_summary_dataset( "Interferometer summary list",
			found, group_id, index, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
		if ( err < 0 ) return -1;

		err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, interferometer[j].summary_buffer );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
		H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );
	}


	// **********************************************
	// *** Free up HDF5 id's and allocated memory ***
	//
	free( discharge.current );
	free( discharge.cathode_anode_voltage );
	free( pressure.RGA_partial_pressure );
	free( magnetic_field.profile );
	free( magnetic_field.supply_current );
	for ( int j=0; j<interferometer_count; j++ )
		free( interferometer[j].density );
	free( interferometer );

	H5Gclose( group_id );
	H5Fclose( file_id );

	return 0;
}


