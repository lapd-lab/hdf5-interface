
Instructions for Compiling with the Pre-Compiled Libraries and DLLs 
for MSVC++ .NET 2003 (version 7.1)
===================================================================

NOTE:
----
The ZLIB 1.2.2 and SZIP 2.0 libraries are NOT included with the HDF5 
software.  You must obtain them separately. You can obtain them from 
the HDF ftp server at:

    ftp://ftp.ncsa.uiuc.edu/HDF/lib-external/zlib/1.2/
    ftp://ftp.ncsa.uiuc.edu/HDF/lib-external/szip/2.0/

If you have problems with the instructions below, please check the
INSTALL_Windows.txt file in the source code for further details.  This
can be found from the HDF5 ftp server location at:

  ftp://ftp.ncsa.uiuc.edu/HDF/HDF5/current/src/unpacked/release_docs/

-------------------------------------------------------------------
   
       
Static Libraries:
----------------

   Bring up MSVC++ .NET.

   Select File->New->Projects->"Visual C++ Projects".  Then select 
    "Managed C++ Empty Project" and enter a name.

   Select File->Open, and open up a C or C++ program.

   Select Project->Properties->Linker->Input
   Enter "hdf5.lib hdf5_hl.lib hdf5_cpp.lib zdll.lib szlib.lib" at the 
          beginning of the "Additional Dependencies" line.

   Go to "Ignore Specific Library"; enter "libcd.lib".

   Select Project->Properties->C/C++->Code Generation->Runtime Library, 
    and choose "Single-threaded".

   Select Tools->Options->Projects->VC++ Directories.
   Select "Include Files" under "Show directories for".
   Enter the paths for the HDF5, szlib and zlib include files.
   Then select "Library Files" under "Show directories for".
   Enter the paths for the HDF5, szlib and zlib libraries.

   Select Build->Compile <program.c>.
   Select Build->Build <xxx.exe>.
   Select Build->Execute <xxx.exe>.

   
DLLs:
----
   Copy the hdf5dll.dll, hdf5_cppdll.dll, zlib1.dll, and szlibdll.dll files 
   from the location in the HDF5, szip, zlib binary distributions to the 
   WINNT\SYSTEM directory.

   Bring up MSVC++ .NET.

   Select File->New->Projects->"Visual C++ Projects".  Then select 
    "Managed C++ Empty Project" and enter a name.
  
   Select Project->Properties->Linker->Input
   Enter "hdf5dll.lib hdf5_cppdll.lib zdll.lib szlibdll.lib" at the 
     beginning of the "Additional Dependencies" line.

   Select Project->Properties->C/C++->Preprocessor
   Under Preprocessor Definitions, add:  _HDF5USEDLL_
   For C++ DLLs, add: HDF5CPP_USEDLL

   Select Project->Properties->C/C++->Code Generation->Runtime Library, 
    and choose "multi-threaded DLL"

   Select Tools->Options->Projects->VC++ Directories.
   Select "Include Files" under "Show directories for".
   Enter the paths for the HDF5, szlib and zlib include files.
   Then select "Library Files" under "Show directories for".
   Enter the paths for the HDF5, szlib and zlib DLL exported libraries.

   Select Build->Compile <program>. (C or C++ program)
   Select Build->Build <xxx.exe>.  
   Select Build->Execute <xxx.exe>.


