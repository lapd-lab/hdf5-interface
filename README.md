# HDF5-interface

C++ application to interface between LabVIEW-based data acquisition system and HDF5.  These were found necessary for 
speed-critical functionality.

*May, 2022*
- Application added to GitLab

*July, 2022*
- Generic MSI handling added (beta version)

## Description
The top-level driver is H5D_Insert_entries.exe.  It is a "Console" application, which means it runs as a shell command
line.  This description is being written in May, 2022 in order to document how this application works to date.  Work to 
generalize the application will begin thereafter.

H5D_Insert_Entries is effectively receiving messages from the data acquisition system (ACQ II written in LabView).  It 
doesn't use a real messaging system, however.  Instead it looks for files that are named according to shot number and
keeps track of ordering that way.

### Usage
`H5D_Insert_entries.exe HDF5-file experiment-set experiment data-run shadow-data-root-folder
    device-count [device-name-list] start-time SIS-crate-root-folder sleep-count-ms data-run-id`

`HDF5-file`: full path of the HDF5 file in which to insert a single shot of device data

`experiment-set experiment data-run`: 3 strings which locate the shot files

`shadow-data-root-folder`: this is where the data acquisition system is storing data, configuration and other information
                           during the data run - this application collates information from here into the HDF5 file

`device-count`: the number of devices in the data run

`[device-name-list]`: the device names surrounded by double quotes, i.e. "SIS 3301" "6K Compumotor"

`start-time`: integer timestamp

`SIS-crate-root-folder`: kludge needed to include SIS crate averaging into the RT translator

`sleep-time-ms`: recommended values: real-time mode -- 1000 ms, manual mode -- 10 ms

`data-run-id`: this identifier is needed for the generic MSI handling, example: "data_run_id 237 Shakedown"

A detailed description of how the console application works is given below.

## Deployment
You can either checkout the code using Git or just download it.  The download is the simpler option because development work on this code is not
actively being done at the LaPD lab, therefore the extra complications of a Git checkout are not necessary.

### How to download the code from GitLab
- Navigate to https://gitlab.com/lapd-lab/hdf5-interface/-/tree/main and confirm that you are viewing the "main" branch
- Click on the Download button and choose "zip" or whatever compression format that you prefer

### Installing the application
- Make sure the previous version of this code is backed up or at least won't get overwritten when you uncompress this download
- Uncompress the download in the location of your preference, which we refer to as `<hdf5-interface>`
- Open `<hdf5-interface>/Console applications/H5D Insert entries/Console.sln` in Microsoft Visual Studio
- Click Build > Rebuild Solution
  - this creates `C:/ACQ II home/HDF5/Current DLL/H5D_Insert_entries.exe`

### Sanity testing the application
The application can be run manually on the command line as shown above.
- Ensure it works both for files from a dataset made before and after generic MSI handling
  - it is intended to be backward-compatible in this way

The application is normally automatically started by `<acq-ii-home>/HDF5 deployments/HDF5 - v1.3/Real-time related/HDF5 RT start C++ code - v1.3.vi` or
whatever the latest version of this VI is.
- Ensure that it works via the VI

## Detailed description
### Glossary
- shot_file: contains header/data for all devices, plus references to configuration information, for a single shot
- devices_file: this is a marker file to signal that all device configuration information has been updated for this shot
- MSI_file: a list of Machine State Information (MSI) types is contained here, for generic MSI handling
  - for each MSI type, there is a byte string file and a corresponding JSON descriptor file
- sequence_lines_file: the data run sequencing "script" lines for this shot are contained here
- error_file: records any error inserting into the HDF5 file

### Algorithm
**H5D_Insert_Entries.main():**
  - initialize filename, counters, etc.
    - notably, the shot number is set to 0

  - loop until timeout or data run complete:
    - sleep for one shot period
    - look for the "shot_file" corresponding to the shot number variable in this application
    - if the shot_file exists and its timestamp corresponds to the <start time> argument:
      - read data run complete flag (referred to as "translation_complete" in the code)

      - look for the "devices_file" corresponding to the shot number variable in this application
      - if the devices_file exists:
        - insert header/data from all devices for this shot into HDF5 file (see insert_devices() below)
        - insert configuration from all devices for this shot into the HDF5 file (see insert_configs() below)

      - look for the "MSI_file" corresponding to the shot number variable in this application
      - if the MSI_file exists:
        - insert generic MSI, for all MSI types, for this shot into HDF5 file (see insert_MSI_generic() below)

      - look for the "sequence_lines_file" corresponding to the shot number variable in this application
      - if the sequence_line_file exists:
        - insert data run sequence information for this shot into HDF5 file (see insert_sequence_lines() below)

      - if any of the inserts into the HDF5 file produced an error:
        - record this error in the "error_file"

      - increment the shot number variable and update the filenames accordingly
        - note that if the shot_file does not exist, this application keeps retrying with the same shot number until it times out
        - however, if any of the other files does not exist (for this shot), this results in a hole in the HDF5 file

**Insert_Devices.insert_devices():**
```
    # earlier devices were custom-handled, then the concept of the remote module dock was introduced which standardized the
    # formatting of the header/data for digitizers and the run-time information for non-digitizers
```

  - loop through all devices in this data run:
    - switch on the device type:  
      - SIS 3301:
        - extract header/data for each channel from the shot_file and write it into the HDF5 file

      - TVS645A:
        - extract header/data for each channel from the shot_file and write it into the HDF5 file

      - 6K Compumotor:
        - extract the run-time information for each probe from the shot_file and write it into the HDF5 file

      - GPIB interface:
        - extract the run-time information for each GPIB channel from the shot_file and write it into the HDF5 file
        
      - Remote device:
        -if the device is the SIS crate:
          - average the SIS crate channels  # this yields a single shot file like a regular digitizer

        - if the device is a digitizer:
          - extract header/data for each channel from the shot_file and write it into the HDF5 file
        - else:
          - determine formatting of run-time information via a descriptor file
          - extract the run-time information from the shot_file and write it into the HDF5 file

**Insert_Configs.insert_configs():**
  - loop through all devices in this data run:
    - switch on the device type:
      - 6K Compumotor:
        - for each probe:
          - read probe config file as specified in the descriptor file (both indexed by shot number) and write to HDF5 file
        - for each motion list:
          - read motion list config file as specified in the descriptor file (both indexed by shot number) and write to HDF5 file
      - all other devices:
        - read config file as specified in the descriptor file (both indexed by shot number) and write to HDF5 file

**Insert_MSI.insert_msi():**
  - MSI is written in a generic way.  Details of this are given in a separate document in this project.

**Insert_Sequence_Lines.insert_sequence_lines():**
  - get the line_count for this shot
  - loop through the sequence lines:
    - read the sequence_line, shot_number, expanded_line_number, line_status and timestamp and write to HDF5 file


## More detailed documentation
TODO: provide links to documentation pages in this project.

## Roadmap
- Upgrade MSI to be generic:
  - analysis phase: June, 2022  - Done
  - beta version: July, 2022  - Done
  - on-site integration and testing  - In Progress
  - complete error handling  - In Progress
  - complete documentation   - In Progress

## Authors and acknowledgment
James Bamber, Steve Vincena

- Please feel free to add the names of others who have contributed to this project.

## Project status
This project has been stable since 2013.  We have the opportunity to improve it in the summer of 2022.
